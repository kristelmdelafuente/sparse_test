// To parse this JSON data, do
//
//     final itemType = itemTypeFromJson(jsonString);

import 'dart:convert';

ItemType itemTypeFromJson(String str) => ItemType.fromJson(json.decode(str));

String itemTypeToJson(ItemType data) => json.encode(data.toJson());

class ItemType {
  int id;
  String name;
  int wearPosition;
  int damage;
  int armor;

  ItemType({
    this.id,
    this.name,
    this.wearPosition,
    this.damage,
    this.armor,
  });

  factory ItemType.fromJson(Map<String, dynamic> json) => ItemType(
        id: int.parse(json["id"]),
        name: json["name"],
        wearPosition: int.parse(json["wearPosition"]),
        damage: json["damage"] == null || json["damage"] == ""
            ? null
            : int.parse(json["damage"]),
        armor: json["armor"] == null || json["armor"] == ""
            ? null
            : int.parse(json["armor"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "wearPosition": wearPosition == null ? null : wearPosition,
        "damage": damage == null ? null : damage,
        "armor": armor == null ? null : armor,
      };
}
