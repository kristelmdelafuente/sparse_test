// To parse this JSON data, do
//
//     final encounter = encounterFromJson(jsonString);

import 'package:sparse/src/models/guard.dart';
import 'dart:convert';

Encounter encounterFromJson(String str) => Encounter.fromJson(json.decode(str));

String encounterToJson(Encounter data) => json.encode(data.toJson());

class Encounter {
  String id;
  int uses;
  String pluralName;
  String message;
  String encounterReplacement;
  bool explored;
  Guard guard;

  Encounter(
      {this.id,
      this.uses,
      this.pluralName,
      this.message,
      this.encounterReplacement,
      this.explored,
      this.guard});

  factory Encounter.fromJson(Map<String, dynamic> json) => Encounter(
        id: json["id"],
        uses: int.parse(json["uses"]),
        pluralName: json["pluralName"],
        message: json["message"] == null || json["message"] == ""
            ? null
            : json["message"],
        encounterReplacement: json["encounterReplacement"] == null ||
                json["encounterReplacement"] == ""
            ? null
            : json["encounterReplacement"],
        explored: false,
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "uses": uses == null ? null : uses,
        "pluralName": pluralName == null ? null : pluralName,
        "message": message == null ? null : message,
        "encounterReplacement":
            encounterReplacement == null ? null : encounterReplacement,
        "explored": explored == null ? null : explored,
      };
}
