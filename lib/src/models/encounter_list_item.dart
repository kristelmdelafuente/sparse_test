import 'dart:convert';

EncounterListItem encounterListItemFromJson(String str) =>
    EncounterListItem.fromJson(json.decode(str));

String encounterListItemToJson(EncounterListItem data) =>
    json.encode(data.toJson());

class EncounterListItem {
  int id;
  int encounterTableId;
  String encounterName;
  int guardTableId;

  EncounterListItem({
    this.id,
    this.encounterTableId,
    this.encounterName,
    this.guardTableId,
  });

  factory EncounterListItem.fromJson(Map<String, dynamic> json) =>
      EncounterListItem(
        id: json["id"] == null ? null : int.parse(json["id"]),
        encounterTableId: json["encounterTableId"] == null
            ? null
            : int.parse(json["encounterTableId"]),
        encounterName:
            json["encounterName"] == null ? null : json["encounterName"],
        guardTableId: json["guardTableId"] == null
            ? null
            : int.parse(json["guardTableId"]),
      );

  Map<String, dynamic> toJson() => {
        "id": encounterTableId == null ? null : encounterTableId,
        "encounterTableId": id == null ? null : id,
        "encounterName": encounterName == null ? null : encounterName,
        "guardTableRef": guardTableId == null ? null : guardTableId,
      };
}
