// To parse this JSON data, do
//
//     final resultTrigger = encounterTriggerFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sparse/src/utility.dart';

List<EncounterTrigger> encounterTriggerFromJson(String str) =>
    List<EncounterTrigger>.from(
        json.decode(str).map((x) => EncounterTrigger.fromJson(x)));

String encounterTriggerToJson(List<EncounterTrigger> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class EncounterTrigger {
  int id;
  String encounterId;
  String trigger;
  int result;
  String interfaceText;
  Color color;

  EncounterTrigger(
      {this.id,
      this.encounterId,
      this.trigger,
      this.result,
      this.interfaceText,
      this.color});

  factory EncounterTrigger.fromJson(Map<String, dynamic> json) =>
      EncounterTrigger(
        id: int.parse(json["id"]),
        encounterId: json["encounterId"],
        trigger: json["trigger"] == null || json["trigger"] == ""
            ? null
            : json["trigger"],
        result: int.parse(json["result"]),
        interfaceText:
            json["interfaceText"] == null || json["interfaceText"] == ""
                ? null
                : json["interfaceText"],
        color: json["color"] == null || json["color"] == ""
            ? null
            : Utils.hexToColor(json["color"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "encounterId": encounterId,
        "trigger": trigger == null ? null : trigger,
        "result": result,
      };
}
