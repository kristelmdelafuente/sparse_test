// To parse this JSON data, do
//
//     final tribesMan = tribesManFromJson(jsonString);

import 'dart:convert';

Person tribesManFromJson(String str) => Person.fromJson(json.decode(str));

String tribesManToJson(Person data) => json.encode(data.toJson());

class Person {
  String head;
  String torso;
  String handled;
  String pet;
  String feet;
  int energy;
  int damage;
  int armor;

  Person({
    this.head,
    this.torso,
    this.handled,
    this.pet,
    this.feet,
    this.energy,
    this.damage,
    this.armor,
  });

  factory Person.fromJson(Map<String, dynamic> json) => Person(
      head: json["head"] == null || json["head"] == "" ? null : json["head"],
      torso:
          json["torso"] == null || json["torso"] == "" ? null : json["torso"],
      handled: json["handled"] == null || json["handled"] == ""
          ? null
          : json["handled"],
      feet: json["feet"] == null || json["feet"] == "" ? null : json["feet"],
      pet: json["pet"] == null || json["pet"] == "" ? null : json["pet"],
      energy: int.parse(json["energy"]),
      damage: 0,
      armor: 0);

  Map<String, dynamic> toJson() => {
//    "head": head == null ? null : head,
//    "torso": torso == null ? null : torso,
        "handled": handled == null ? null : handled,
        "pet": pet == null ? null : pet,
//    "feet": feet == null ? null : feet,
//    "pet": pet == null ? null : pet,
        "energy": energy == null ? null : energy,
      };
}
