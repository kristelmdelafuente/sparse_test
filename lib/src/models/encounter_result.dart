// To parse this JSON data, do
//
//     final encounterResult = encounterResultFromJson(jsonString);

import 'dart:convert';

List<EncounterResult> encounterResultFromJson(String str) =>
    List<EncounterResult>.from(
        json.decode(str).map((x) => EncounterResult.fromJson(x)));

class EncounterResult {
  int id;
  int groupId;
  double chance;
  int resultItemsId;
  int usesConsumed;
  int energyConsumed;
  String encounterCreated;
  int backgroundImg;
  String message;

  EncounterResult({
    this.id,
    this.groupId,
    this.chance,
    this.resultItemsId,
    this.usesConsumed,
    this.energyConsumed,
    this.encounterCreated,
    this.backgroundImg,
    this.message,
  });

  factory EncounterResult.fromJson(Map<String, dynamic> json) =>
      EncounterResult(
        id: json["id"] == null ? null : int.parse(json["id"]),
        groupId: json["groupId"] == null ? null : int.parse(json["groupId"]),
        chance: json["chance"] == null
            ? null
            : num.tryParse(json["chance"])?.toDouble(),
        resultItemsId: json["resultItemsId"] == null
            ? null
            : int.parse(json["resultItemsId"]),
        usesConsumed: json["usesConsumed"] == null
            ? null
            : int.parse(json["usesConsumed"]),
        energyConsumed: json["energyConsumed"] == null
            ? null
            : int.parse(json["energyConsumed"]),
        encounterCreated:
            json["encounterCreated"] == null || json["encounterCreated"] == ""
                ? null
                : json["encounterCreated"],
        backgroundImg:
            json["backgroundImg"] == null || json["backgroundImg"] == ""
                ? null
                : int.parse(json["backgroundImg"]),
        message: json["message"] == null || json["message"] == ""
            ? null
            : json["message"],
      );
}
