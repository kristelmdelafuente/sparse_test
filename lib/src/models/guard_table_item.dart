// To parse this JSON data, do
//
//     final guardTable = guardTableFromJson(jsonString);

import 'dart:convert';

GuardTableItem guardTableItemFromJson(String str) =>
    GuardTableItem.fromJson(json.decode(str));

String guardTableItemToJson(GuardTableItem data) => json.encode(data.toJson());

class GuardTableItem {
  int id;
  int tableId;
  String guardId;
  int number;

  GuardTableItem({
    this.id,
    this.tableId,
    this.guardId,
    this.number,
  });

  factory GuardTableItem.fromJson(Map<String, dynamic> json) => GuardTableItem(
        id: int.parse(json["id"]),
        tableId: int.parse(json["tableId"]),
        guardId: json["guardId"],
        number: int.parse(json["number"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "tableId": tableId == null ? null : tableId,
        "guardId": guardId == null ? null : guardId,
        "numOfGuards": number == null ? null : number,
      };
}
