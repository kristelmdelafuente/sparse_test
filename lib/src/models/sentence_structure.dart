// To parse this JSON data, do
//
//     final sentenceStructure = sentenceStructureFromJson(jsonString);

import 'dart:convert';

List<SentenceStructure> sentenceStructureFromJson(String str) =>
    List<SentenceStructure>.from(
        json.decode(str).map((x) => SentenceStructure.fromJson(x)));

String sentenceStructureToJson(List<SentenceStructure> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SentenceStructure {
  String personName;
  String equippedItem;
  String handledItem;
  String followingPet;
  String verb;
  String hasEncounter;
  String hasEncounterCont;
  String noEncounter;
  String noEncounterCont;
  String worldName;
  String droppedItems;
  String droppedItemsCont;
  String deadState;

  SentenceStructure({
    this.personName,
    this.equippedItem,
    this.handledItem,
    this.followingPet,
    this.verb,
    this.hasEncounter,
    this.hasEncounterCont,
    this.noEncounter,
    this.noEncounterCont,
    this.worldName,
    this.droppedItems,
    this.droppedItemsCont,
    this.deadState,
  });

  factory SentenceStructure.fromJson(Map<String, dynamic> json) =>
      SentenceStructure(
        personName: json["personName"] == null ? '' : json["personName"],
        equippedItem: json["equippedItem"] == null ? '' : json["equippedItem"],
        handledItem: json["handledItem"] == null ? '' : json["handledItem"],
        followingPet: json["followingPet"] == null ? '' : json["followingPet"],
        verb: json["verb"] == null ? null : json["verb"],
        hasEncounter: json["hasEncounter"] == null ? '' : json["hasEncounter"],
        hasEncounterCont:
            json["hasEncounterCont"] == null ? '' : json["hasEncounterCont"],
        noEncounter: json["noEncounter"] == null ? '' : json["noEncounter"],
        noEncounterCont:
            json["noEncounterCont"] == null ? '' : json["noEncounterCont"],
        worldName: json["worldName"] == null ? '' : json["worldName"],
        droppedItems: json["droppedItems"] == null ? '' : json["droppedItems"],
        droppedItemsCont:
            json["droppedItemsCont"] == null ? '' : json["droppedItemsCont"],
        deadState: json["deadState"] == null ? '' : json["deadState"],
      );

  Map<String, dynamic> toJson() => {
        "personName": personName == null ? null : personName,
        "equippedItem": equippedItem == null ? null : equippedItem,
        "handledItem": handledItem == null ? null : handledItem,
        "followingPet": followingPet == null ? null : followingPet,
        "verb": verb == null ? null : verb,
        "hasEncounter": hasEncounter == null ? null : hasEncounter,
        "hasEncounterCont": hasEncounterCont == null ? null : hasEncounterCont,
        "noEncounter": noEncounter == null ? null : noEncounter,
        "noEncounterCont": noEncounterCont == null ? null : noEncounterCont,
        "worldName": worldName == null ? null : worldName,
        "droppedItems": droppedItems == null ? null : droppedItems,
        "droppedItemsCont": droppedItemsCont == null ? null : droppedItemsCont,
        "deadState": deadState == null ? null : deadState,
      };
}
