// To parse this JSON data, do
//
//     final interfaceWords = interfaceWordsFromJson(jsonString);

import 'dart:convert';

List<InterfaceWords> interfaceWordsFromJson(String str) =>
    List<InterfaceWords>.from(
        json.decode(str).map((x) => InterfaceWords.fromJson(x)));

String interfaceWordsToJson(List<InterfaceWords> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class InterfaceWords {
  String id;
  String text;

  InterfaceWords({
    this.id,
    this.text,
  });

  factory InterfaceWords.fromJson(Map<String, dynamic> json) => InterfaceWords(
        id: json["id"] == null ? null : json["id"],
        text: json["text"] == null ? null : json["text"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "text": text == null ? null : text,
      };
}
