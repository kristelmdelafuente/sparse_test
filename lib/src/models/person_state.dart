// To parse this JSON data, do
//
//     final personState = personStateFromJson(jsonString);

import 'dart:convert';

List<PersonState> personStateFromJson(String str) => List<PersonState>.from(
    json.decode(str).map((x) => PersonState.fromJson(x)));

String personStateToJson(List<PersonState> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PersonState {
  int energy;
  String status;

  PersonState({
    this.energy,
    this.status,
  });

  factory PersonState.fromJson(Map<String, dynamic> json) => PersonState(
        energy: json["energy"] == null ? null : int.parse(json["energy"]),
        status: json["status"] == null ? "" : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "energy": energy == null ? null : energy,
        "status": status == null ? null : status,
      };
}
