import 'dart:convert';

EncounterTableItem encounterTableItemFromJson(String str) =>
    EncounterTableItem.fromJson(json.decode(str));

String encounterItemTableToJson(EncounterTableItem data) =>
    json.encode(data.toJson());

class EncounterTableItem {
  int id;
  int numberEncounters;

  EncounterTableItem({
    this.id,
    this.numberEncounters,
  });

  factory EncounterTableItem.fromJson(Map<String, dynamic> json) =>
      EncounterTableItem(
        id: json["id"] == null ? null : int.parse(json["id"]),
        numberEncounters: json["numberEncounters"] == null
            ? null
            : int.parse(json["numberEncounters"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "numberEncounters": numberEncounters == null ? null : numberEncounters,
      };
}
