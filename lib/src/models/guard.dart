// To parse this JSON data, do
//
//     final guard = guardFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sparse/src/utility.dart';

Guard guardFromJson(String str) => Guard.fromJson(json.decode(str));

String guardToJson(Guard data) => json.encode(data.toJson());

class Guard {
  String id;
  int energy;
  int damage;
  int armor;
  String masterItemId;
  String itemId;
  Color color;
  String message;

  Guard(
      {this.id,
      this.energy,
      this.damage,
      this.armor,
      this.masterItemId,
      this.itemId,
      this.color,
      this.message});

  factory Guard.fromJson(Map<String, dynamic> json) => Guard(
        id: json["id"],
        energy: json["energy"] == null ? null : int.parse(json["energy"]),
        damage: json["damage"] == null ? null : int.parse(json["damage"]),
        armor: json["armor"] == null ? null : int.parse(json["armor"]),
        masterItemId: json["masterItemId"] == null || json["masterItemId"] == ""
            ? null
            : json["masterItemId"],
        itemId: json["itemId"] == null || json["itemId"] == ""
            ? null
            : json["itemId"],
        color: json["color"] == null || json["color"] == ""
            ? null
            : Utils.hexToColor(json["color"]),
        message: json["message"] == null || json["message"] == ""
            ? null
            : json["message"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "energy": energy == null ? null : energy,
        "damage": damage == null ? null : damage,
        "armor": armor == null ? null : armor,
        "masterItemRef": masterItemId,
        "itemId": itemId,
        "message": message == null ? null : message,
      };
}
