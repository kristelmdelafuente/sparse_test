// To parse this JSON data, do
//
//     final recipe = recipeFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sparse/src/utility.dart';

Recipe recipeFromJson(String str) => Recipe.fromJson(json.decode(str));

String recipeToJson(Recipe data) => json.encode(data.toJson());

class Recipe {
  String carried;
  String target;
  String itemCreated;
  Color color;
  int carriedConsumed;
  int targetConsumed;
  int energyCost;
  String message;

  Recipe(
      {this.carried,
      this.target,
      this.itemCreated,
      this.color,
      this.carriedConsumed,
      this.targetConsumed,
      this.energyCost,
      this.message});

  factory Recipe.fromJson(Map<String, dynamic> json) => Recipe(
        carried: json["carried"] == null || json["carried"] == ""
            ? null
            : json["carried"],
        target: json["target"],
        itemCreated: json["itemCreated"] == null || json["itemCreated"] == ""
            ? null
            : json["itemCreated"],
        color: json["color"] == null || json["color"] == ""
            ? null
            : Utils.hexToColor(json["color"]),
        carriedConsumed: json["carriedConsumed"] == null
            ? null
            : int.parse(json["carriedConsumed"]),
        targetConsumed: json["targetConsumed"] == null
            ? null
            : int.parse(json["targetConsumed"]),
        energyCost:
            json["energyCost"] == null ? null : int.parse(json["energyCost"]),
        message: json["message"] == null || json["message"] == ""
            ? null
            : json["message"],
      );

  Map<String, dynamic> toJson() => {
        "carried": carried == null ? null : carried,
        "target": target == null ? null : target,
        "itemCreated": itemCreated == null ? null : itemCreated,
        "carriedConsumed": carriedConsumed == null ? null : carriedConsumed,
        "targetConsumed": targetConsumed == null ? null : targetConsumed,
        "energyCost": energyCost == null ? null : energyCost,
        "message": message == null ? null : message,
      };
}
