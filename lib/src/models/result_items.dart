// To parse this JSON data, do
//
//     final resultItems = resultItemsFromJson(jsonString);

import 'dart:convert';

List<ResultItems> resultItemsFromJson(String str) => List<ResultItems>.from(
    json.decode(str).map((x) => ResultItems.fromJson(x)));

String resultItemsToJson(List<ResultItems> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ResultItems {
  int id;
  int resultItemsId;
  String itemId;

  ResultItems({
    this.id,
    this.resultItemsId,
    this.itemId,
  });

  factory ResultItems.fromJson(Map<String, dynamic> json) => ResultItems(
        id: json["id"] == null ? null : int.parse(json["id"]),
        resultItemsId: json["resultItemsId"] == null
            ? null
            : int.parse(json["resultItemsId"]),
        itemId: json["itemId"] == null ? null : json["itemId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "resultItemsId": resultItemsId == null ? null : resultItemsId,
        "itemId": itemId == null ? null : itemId,
      };
}
