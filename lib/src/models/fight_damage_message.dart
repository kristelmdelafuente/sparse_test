// To parse this JSON data, do
//
//     final fightDamageMessage = fightDamageMessageFromJson(jsonString);

import 'dart:convert';

FightDamageMessage fightDamageMessageFromJson(String str) =>
    FightDamageMessage.fromJson(json.decode(str));

String fightDamageMessageToJson(FightDamageMessage data) =>
    json.encode(data.toJson());

class FightDamageMessage {
  int id;
  int personDamage;
  int guardDamage;
  String guard;
  String message;

  FightDamageMessage({
    this.id,
    this.personDamage,
    this.guardDamage,
    this.guard,
    this.message,
  });

  factory FightDamageMessage.fromJson(Map<String, dynamic> json) =>
      FightDamageMessage(
        id: int.parse(json["id"]),
        personDamage: int.parse(json["personDamage"]),
        guardDamage: int.parse(json["guardDamage"]),
        guard: json["guard"],
        message: json["message"] == null || json["message"] == ""
            ? null
            : json["message"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "personDamage": personDamage,
        "guardDamage": guardDamage,
        "guard": guard,
        "message": message == null ? null : message,
      };
}
