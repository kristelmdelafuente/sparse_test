// To parse this JSON data, do
//
//     final item = itemFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/cupertino.dart';

Item itemFromJson(String str) => Item.fromJson(json.decode(str));

String itemToJson(Item data) => json.encode(data.toJson());

class Item {
  String id;
  String pluralName;
  int type;
  int quantity;
  Color textColor;

  Item({this.id, this.pluralName, this.type, this.quantity, this.textColor});

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["id"],
        pluralName: json["pluralName"],
        type: int.parse(json["type"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "pluralName": pluralName,
        "type": type,
      };
}
