import 'package:sparse/src/models/encounter.dart';

class SparseWorld {
  List<Encounter> encounters;
  List<String> droppedItems;

  SparseWorld({this.encounters, this.droppedItems});
}
