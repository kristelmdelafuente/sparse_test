import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:sparse/src/models/encounter_list_item.dart';
import 'package:sparse/src/models/encounter.dart';
import 'package:sparse/src/models/encounter_table_item.dart';
import 'package:sparse/src/models/fight_damage_message.dart';
import 'package:sparse/src/models/guard_table_item.dart';
import 'package:sparse/src/models/guard.dart';
import 'package:sparse/src/models/encounter_result.dart';
import 'package:sparse/src/models/interface_words.dart';
import 'package:sparse/src/models/item.dart';
import 'package:sparse/src/models/item_type.dart';
import 'package:sparse/src/models/person.dart';
import 'package:sparse/src/models/person_state.dart';
import 'package:sparse/src/models/recipe.dart';
import 'package:sparse/src/models/result_items.dart';
import 'package:sparse/src/models/encounter_trigger.dart';
//import 'package:sparse/src/models/sentence_structure.dart';
import 'package:sparse/src/resources/game_data.dart';
import 'package:sparse/src/utility.dart';

class ApiProvider {
  bool isLocal = false;
  Dio _dio = Dio();

  ApiProvider() {
    _dio.interceptors.add(PrettyDioLogger());
  }

  Future<Person> getPersonData() async {
    if (isLocal) {
      return personData
          .map((encounterTable) => new Person.fromJson(encounterTable))
          .toList()
          .first;
    } else {
      try {
        var response = await _dio.get("${Sheets.BASE_URL}${Sheets.TAB_PERSON}");
        return (response.data as List)
            .map((encounterTable) => new Person.fromJson(encounterTable))
            .toList()
            .first;
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }

  Future<List<EncounterListItem>> getEncounterListData() async {
    if (isLocal) {
      return encounterListData
          .map((encounterTable) =>
              new EncounterListItem.fromJson(encounterTable))
          .toList();
    } else {
      try {
        var response =
            await _dio.get("${Sheets.BASE_URL}${Sheets.TAB_ENCOUNTER_LIST}");
        return (response.data as List)
            .map((encounterTable) =>
                new EncounterListItem.fromJson(encounterTable))
            .toList();
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }

  Future<List<EncounterTableItem>> getEncounterTableData() async {
    if (isLocal) {
      return encounterTableData
          .map((encounterTable) =>
              new EncounterTableItem.fromJson(encounterTable))
          .toList();
    } else {
      try {
        var response =
            await _dio.get("${Sheets.BASE_URL}${Sheets.TAB_ENCOUNTER_TABLE}");
        return (response.data as List)
            .map((encounterTable) =>
                new EncounterTableItem.fromJson(encounterTable))
            .toList();
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }

  Future<List<Encounter>> getEncountersData() async {
    if (isLocal) {
      return encountersData
          .map((encounterTable) => new Encounter.fromJson(encounterTable))
          .toList();
    } else {
      try {
        var response =
            await _dio.get("${Sheets.BASE_URL}${Sheets.TAB_ENCOUNTERS}");
        return (response.data as List)
            .map((encounterTable) => new Encounter.fromJson(encounterTable))
            .toList();
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }

  Future<List<GuardTableItem>> getGuardTableData() async {
    if (isLocal) {
      return guardTableData
          .map((guardTable) => new GuardTableItem.fromJson(guardTable))
          .toList();
    } else {
      try {
        var response =
            await _dio.get("${Sheets.BASE_URL}${Sheets.TAB_GUARD_TABLES}");
        return (response.data as List)
            .map(
                (encounterTable) => new GuardTableItem.fromJson(encounterTable))
            .toList();
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }

  Future<List<Guard>> getGuardData() async {
    if (isLocal) {
      return guardsData.map((guard) => new Guard.fromJson(guard)).toList();
    } else {
      try {
        var response = await _dio.get("${Sheets.BASE_URL}${Sheets.TAB_GUARDS}");
        return (response.data as List)
            .map((encounterTable) => new Guard.fromJson(encounterTable))
            .toList();
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }

  Future<List<EncounterResult>> getEncounterResultsData() async {
    if (isLocal) {
      return resultsData
          .map((result) => new EncounterResult.fromJson(result))
          .toList();
    } else {
      try {
        var response =
            await _dio.get("${Sheets.BASE_URL}${Sheets.TAB_RESULTS}");
        return (response.data as List)
            .map((encounterTable) =>
                new EncounterResult.fromJson(encounterTable))
            .toList();
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }

  Future<List<ResultItems>> getResultItemsData() async {
    if (isLocal) {
      return resultItemsData
          .map((resultItems) => new ResultItems.fromJson(resultItems))
          .toList();
    } else {
      try {
        var response =
            await _dio.get("${Sheets.BASE_URL}${Sheets.TAB_RESULT_ITEMS}");
        return (response.data as List)
            .map((encounterTable) => new ResultItems.fromJson(encounterTable))
            .toList();
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }

  Future<List<Item>> getItemsData() async {
    if (isLocal) {
      return itemsData.map((item) => new Item.fromJson(item)).toList();
    } else {
      try {
        var response = await _dio.get("${Sheets.BASE_URL}${Sheets.TAB_ITEMS}");
        return (response.data as List)
            .map((encounterTable) => new Item.fromJson(encounterTable))
            .toList();
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }

  Future<List<ItemType>> getItemTypesData() async {
    if (isLocal) {
      return itemTypesData
          .map((itemType) => new ItemType.fromJson(itemType))
          .toList();
    } else {
      try {
        var response =
            await _dio.get("${Sheets.BASE_URL}${Sheets.TAB_ITEM_TYPES}");
        return (response.data as List)
            .map((encounterTable) => new ItemType.fromJson(encounterTable))
            .toList();
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }

  Future<List<Recipe>> getRecipesData() async {
    if (isLocal) {
      return recipesData.map((recipe) => new Recipe.fromJson(recipe)).toList();
    } else {
      try {
        var response =
            await _dio.get("${Sheets.BASE_URL}${Sheets.TAB_RECIPES}");
        return (response.data as List)
            .map((encounterTable) => new Recipe.fromJson(encounterTable))
            .toList();
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }

  Future<List<PersonState>> getPersonStateData() async {
    if (isLocal) {
      return personStateData
          .map((recipe) => new PersonState.fromJson(recipe))
          .toList();
    } else {
      try {
        var response =
            await _dio.get("${Sheets.BASE_URL}${Sheets.TAB_PERSON_STATE}");
        return (response.data as List)
            .map((encounterTable) => new PersonState.fromJson(encounterTable))
            .toList();
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }

  Future<List<FightDamageMessage>> getFightDamageMessageData() async {
    if (isLocal) {
      return fightDamageMessageData
          .map((fightDamageMessage) =>
              new FightDamageMessage.fromJson(fightDamageMessage))
          .toList();
    } else {
      try {
        var response = await _dio
            .get("${Sheets.BASE_URL}${Sheets.TAB_FIGHT_DAMAGE_MESSAGE}");
        return (response.data as List)
            .map((fightDamageMessage) =>
                new FightDamageMessage.fromJson(fightDamageMessage))
            .toList();
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }

//  Future<SentenceStructure> getSentenceStructureData() async {
//    if (isLocal){
//      return sentenceStructureData.map((recipe) => new SentenceStructure.fromJson(recipe)).toList().first;
//    } else {
//      try {
//        var response = await _dio.get("${Sheets.BASE_URL}${Sheets.TAB_SENTENCE_STRUCTURE}");
//        return (response.data as List).map((encounterTable) => new SentenceStructure.fromJson(encounterTable)).toList().first;
//      } on DioError catch (e) {
//        print('REQUEST ERROR: $e');
//        rethrow;
//      } catch (e){
//        print('PARSE ERROR: $e');
//        rethrow;
//      }
//    }
//  }

  Future<List<InterfaceWords>> getInterfaceWords() async {
    if (isLocal) {
      return interfaceWordsData
          .map((recipe) => new InterfaceWords.fromJson(recipe))
          .toList();
    } else {
      try {
        var response =
            await _dio.get("${Sheets.BASE_URL}${Sheets.TAB_INTERFACE_WORDS}");
        return (response.data as List)
            .map(
                (encounterTable) => new InterfaceWords.fromJson(encounterTable))
            .toList();
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }

  Future<List<EncounterTrigger>> getEncounterTriggers() async {
    if (isLocal) {
      return resultTriggersData
          .map((recipe) => new EncounterTrigger.fromJson(recipe))
          .toList();
    } else {
      try {
        var response = await _dio
            .get("${Sheets.BASE_URL}${Sheets.TAB_ENCOUNTER_TRIGGERS}");
        return (response.data as List)
            .map((encounterTable) =>
                new EncounterTrigger.fromJson(encounterTable))
            .toList();
      } on DioError catch (e) {
        print('REQUEST ERROR: $e');
        rethrow;
      } catch (e) {
        print('PARSE ERROR: $e');
        rethrow;
      }
    }
  }
}
