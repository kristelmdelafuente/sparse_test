List<Map<String, String>> encounterListData = [
  {
    "id": "0",
    "encounterTableId": "1",
    "encounterName": "Flock",
    "guardTableId": "0"
  },
  {
    "id": "1",
    "encounterTableId": "1",
    "encounterName": "Dear",
    "guardTableId": "0"
  },
  {
    "id": "2",
    "encounterTableId": "1",
    "encounterName": "Grove",
    "guardTableId": "0"
  },
  {
    "id": "3",
    "encounterTableId": "1",
    "encounterName": "Outcropping",
    "guardTableId": "0"
  },
  {
    "id": "4",
    "encounterTableId": "1",
    "encounterName": "Pond",
    "guardTableId": "0"
  },
  {
    "id": "5",
    "encounterTableId": "2",
    "encounterName": "Dear",
    "guardTableId": "1"
  },
  {
    "id": "6",
    "encounterTableId": "2",
    "encounterName": "Grove",
    "guardTableId": "1"
  },
  {
    "id": "7",
    "encounterTableId": "2",
    "encounterName": "Outcropping",
    "guardTableId": "1"
  },
  {
    "id": "8",
    "encounterTableId": "3",
    "encounterName": "Mountain",
    "guardTableId": "2"
  },
  {
    "id": "9",
    "encounterTableId": "3",
    "encounterName": "Forrest",
    "guardTableId": "2"
  },
  {
    "id": "10",
    "encounterTableId": "3",
    "encounterName": "River",
    "guardTableId": "2"
  },
  {
    "id": "11",
    "encounterTableId": "3",
    "encounterName": "Bison",
    "guardTableId": "0"
  },
  {
    "id": "12",
    "encounterTableId": "4",
    "encounterName": "White Bison",
    "guardTableId": "0"
  },
  {
    "id": "13",
    "encounterTableId": "4",
    "encounterName": "Migrant Family",
    "guardTableId": "0"
  }
];

List<Map<String, String>> personData = [
  {
    "handled": "Uncooked Meat",
    "head": "",
    "torso": "",
    "feet": "",
    "pet": "",
    "energy": "1"
  }
];

List<Map<String, String>> encountersData = [
  {
    "id": "Flock",
    "uses": "3",
    "pluralName": "Flocks",
    "message": "The person wadered to the flock."
  },
  {"id": "Dear", "uses": "2", "pluralName": "Dears", "message": null},
  {"id": "Grove", "uses": "9", "pluralName": "Groves", "message": null},
  {
    "id": "Outcropping",
    "uses": "5",
    "pluralName": "Outcroppings",
    "message": null
  },
  {"id": "Pond", "uses": "13", "pluralName": "Ponds", "message": null},
  {"id": "Mountain", "uses": "30", "pluralName": "Mountains", "message": null},
  {"id": "Forrest", "uses": "30", "pluralName": "Forrests", "message": null},
  {"id": "River", "uses": "999", "pluralName": "Rivers", "message": null},
  {"id": "Bison", "uses": "1", "pluralName": "Bisons", "message": null},
  {
    "id": "White Bison",
    "uses": "1",
    "pluralName": "White Bisons",
    "message": null
  },
  {
    "id": "Migrant Family",
    "uses": "1",
    "pluralName": "Migrant Families",
    "message": null
  },
  {
    "id": "Trade Outpost",
    "uses": "1",
    "pluralName": "Trade Outposts",
    "message": null
  },
  {
    "id": "Healing Shrine",
    "uses": "1",
    "pluralName": "Healing Shrines",
    "message": null
  },
  {
    "id": "Waring Clan",
    "uses": "1",
    "pluralName": "Waring Clans",
    "message": null
  },
  {"id": "Warchild", "uses": "1", "pluralName": "Warchildren", "message": null}
];

List<Map<String, String>> encounterTableData = [
  {"id": "1", "numberEncounters": "5"},
  {"id": "2", "numberEncounters": "3"},
  {"id": "3", "numberEncounters": "4"},
  {"id": "4", "numberEncounters": "2"}
];

List<Map<String, String>> guardTableData = [
  {"id": "0", "tableId": "1", "guardId": "Jackel", "number": "2"},
  {"id": "1", "tableId": "2", "guardId": "Wolf", "number": "1"},
  {"id": "2", "tableId": "2", "guardId": "Tiger", "number": "1"},
  {"id": "3", "tableId": "2", "guardId": "Bear", "number": "1"}
];

List<Map<String, String>> guardsData = [
  {
    "id": "Jackel",
    "energy": "1",
    "damage": "1",
    "armor": "1",
    "masterItemId": "",
    "itemId": "",
    "message":
        "The person comes into a clearing with a wolf. \nThey circle each other for a few minutes, the wolf growls, barks and leaps forward frightening and bitting the person. The person runs from the clearing lightly bleeding."
  },
  {
    "id": "Wolf",
    "energy": "2",
    "damage": "1",
    "armor": "1",
    "masterItemId": "Uncooked Meat",
    "itemId": "Wolf Pet",
    "message": null
  },
  {
    "id": "Tiger",
    "energy": "2",
    "damage": "2",
    "armor": "1",
    "masterItemId": "Stick with string and feather",
    "itemId": "Tiger Pet",
    "message": null
  },
  {
    "id": "Bear",
    "energy": "2",
    "damage": "2",
    "armor": "2",
    "masterItemId": "Dream Catcher",
    "itemId": "Bear Pet",
    "message": null
  }
];

List<Map<String, String>> resultsData = [
  {
    "id": "1",
    "groupId": "1",
    "chance": "100",
    "resultItemsId": "0",
    "usesConsumed": "0",
    "energyConsumed": "1",
    "encounterCreated": null,
    "message": "The result is 1. 100% chance and consumed 1 energy."
  },
  {
    "id": "2",
    "groupId": "2",
    "chance": "100",
    "resultItemsId": "1",
    "usesConsumed": "1",
    "energyConsumed": "1",
    "encounterCreated": null,
    "message": null
  },
  {
    "id": "3",
    "groupId": "3",
    "chance": "50",
    "resultItemsId": "2",
    "usesConsumed": "1",
    "energyConsumed": "1",
    "encounterCreated": null,
    "message": null
  },
  {
    "id": "4",
    "groupId": "3",
    "chance": "50",
    "resultItemsId": "3",
    "usesConsumed": "1",
    "energyConsumed": "1",
    "encounterCreated": null,
    "message": null
  },
  {
    "id": "5",
    "groupId": "4",
    "chance": "100",
    "resultItemsId": "4",
    "usesConsumed": "1",
    "energyConsumed": "0",
    "encounterCreated": null,
    "message": null
  },
  {
    "id": "6",
    "groupId": "5",
    "chance": "100",
    "resultItemsId": "5",
    "usesConsumed": "1",
    "energyConsumed": "0",
    "encounterCreated": "Dear",
    "message":
        "The person enters the grove with an Axe and chops down one of the small trees. They come out with a thick branch to use in future endeavors."
  },
  {
    "id": "7",
    "groupId": "6",
    "chance": "100",
    "resultItemsId": "6",
    "usesConsumed": "1",
    "energyConsumed": "0",
    "encounterCreated": null,
    "message": null
  },
  {
    "id": "8",
    "groupId": "7",
    "chance": "100",
    "resultItemsId": "7",
    "usesConsumed": "1",
    "energyConsumed": "0",
    "encounterCreated": null,
    "message": null
  },
  {
    "id": "9",
    "groupId": "8",
    "chance": "100",
    "resultItemsId": "0",
    "usesConsumed": "1",
    "energyConsumed": "-4",
    "encounterCreated": null,
    "message": null
  }
];

List<Map<String, String>> resultTriggersData = [
  {"id": "1", "encounterId": "Flock", "trigger": "", "result": "1"},
  {"id": "2", "encounterId": "Flock", "trigger": "Net", "result": "2"},
  {"id": "3", "encounterId": "Dear", "trigger": "", "result": "1"},
  {"id": "4", "encounterId": "Dear", "trigger": "weapon_1", "result": "3"},
  {"id": "5", "encounterId": "Dear", "trigger": "weapon_2", "result": "4"},
  {"id": "6", "encounterId": "Grove", "trigger": "", "result": "1"},
  {"id": "7", "encounterId": "Grove", "trigger": "tool_1", "result": "5"},
  {"id": "8", "encounterId": "Outcropping", "trigger": "", "result": "6"},
  {"id": "9", "encounterId": "Pond", "trigger": "", "result": "7"},
  {"id": "10", "encounterId": "Pond", "trigger": "3", "result": "8"},
  {"id": "11", "encounterId": "Mountain", "trigger": "", "result": "6"},
  {"id": "12", "encounterId": "Forrest", "trigger": "", "result": "5"},
  {"id": "13", "encounterId": "River", "trigger": "", "result": "8"},
  {"id": "14", "encounterId": "Bison", "trigger": "", "result": "1"},
  {"id": "15", "encounterId": "White Bison", "trigger": "", "result": "1"},
  {"id": "16", "encounterId": "Migrant Family", "trigger": "", "result": "1"},
  {"id": "17", "encounterId": "Trade Outpost", "trigger": "", "result": "1"},
  {"id": "18", "encounterId": "Healing Shrine", "trigger": "", "result": "1"},
  {"id": "19", "encounterId": "Waring Clan", "trigger": "", "result": "1"},
  {"id": "20", "encounterId": "Warchild", "trigger": "", "result": "1"},
  {"id": "21", "encounterId": "Thicket", "trigger": "", "result": "9"},
  {"id": "22", "encounterId": "Small grove", "trigger": "", "result": "10"},
  {
    "id": "23",
    "encounterId": "Tree filled glen",
    "trigger": "",
    "result": "11"
  },
  {"id": "24", "encounterId": "Wooded vale", "trigger": "", "result": "12"},
  {
    "id": "25",
    "encounterId": "Small stone outcropping",
    "trigger": "",
    "result": "13"
  },
  {
    "id": "26",
    "encounterId": "Stone filled field",
    "trigger": "",
    "result": "14"
  },
  {"id": "27", "encounterId": "Rocky knoll", "trigger": "", "result": "15"},
  {
    "id": "28",
    "encounterId": "Cobblestone crag",
    "trigger": "",
    "result": "16"
  },
  {"id": "29", "encounterId": "Wetlands", "trigger": "", "result": "17"},
  {"id": "30", "encounterId": "Pond", "trigger": "", "result": "18"},
  {"id": "31", "encounterId": "Wandering Doe", "trigger": "", "result": "19"},
  {
    "id": "32",
    "encounterId": "Wandering Doe",
    "trigger": "weapon_1",
    "result": "20"
  },
  {"id": "34", "encounterId": "Flock of birds", "trigger": "", "result": "21"},
  {
    "id": "35",
    "encounterId": "Flock of birds",
    "trigger": "Net",
    "result": "22"
  },
  {"id": "36", "encounterId": "Forested gorge", "trigger": "", "result": "23"},
  {
    "id": "37",
    "encounterId": "Tree covered bluff",
    "trigger": "",
    "result": "24"
  },
  {
    "id": "38",
    "encounterId": "Stone strewn mesa",
    "trigger": "",
    "result": "25"
  },
  {"id": "39", "encounterId": "Rocky highland", "trigger": "", "result": "26"},
  {"id": "40", "encounterId": "Orchard", "trigger": "", "result": "27"},
  {"id": "41", "encounterId": "Duck Pond", "trigger": "", "result": "28"},
  {"id": "42", "encounterId": "Duck Pond", "trigger": "Net", "result": "29"},
  {
    "id": "43",
    "encounterId": "Elephant graveyard",
    "trigger": "",
    "result": "30"
  },
  {
    "id": "44",
    "encounterId": "Elephant graveyard",
    "trigger": "tool_1",
    "result": "31"
  },
  {
    "id": "45",
    "encounterId": "Outcropping",
    "trigger": "Uncooked Meat",
    "result": "8"
  }
];

List<Map<String, String>> resultItemsData = [
  {"id": "1", "resultItemsId": "1", "itemId": "Feather"},
  {"id": "2", "resultItemsId": "2", "itemId": "Leather"},
  {"id": "3", "resultItemsId": "3", "itemId": "Uncooked Meat"},
  {"id": "4", "resultItemsId": "4", "itemId": "Leather"},
  {"id": "5", "resultItemsId": "4", "itemId": "Uncooked Meat"},
  {"id": "6", "resultItemsId": "5", "itemId": "Forrest Plant"},
  {"id": "7", "resultItemsId": "6", "itemId": "Stone"},
  {"id": "8", "resultItemsId": "7", "itemId": "Wetlands Plant"}
];

List<Map<String, String>> itemsData = [
  {
    "id": "Forrest Plant",
    "pluralName": "Forrest Plants",
    "type": "0",
    "craftStep": "1"
  },
  {"id": "Stone", "pluralName": "Stones", "type": "4", "craftStep": "1"},
  {
    "id": "Wetlands Plant",
    "pluralName": "Wetlands Plants",
    "type": "0",
    "craftStep": "1"
  },
  {
    "id": "Uncooked Small Fish",
    "pluralName": "Uncooked Small Fishes",
    "type": "0",
    "craftStep": "1"
  },
  {
    "id": "Uncooked Medium Fish",
    "pluralName": "Uncooked Medium Fishes",
    "type": "0",
    "craftStep": "1"
  },
  {
    "id": "Uncooked Large Fish",
    "pluralName": "Uncooked Large Fishes",
    "type": "0",
    "craftStep": "1"
  },
  {
    "id": "Uncooked Meat",
    "pluralName": "Uncooked Meats",
    "type": "0",
    "craftStep": "1"
  },
  {"id": "Leather", "pluralName": "Leathers", "type": "0", "craftStep": "3"},
  {
    "id": "Red Berry",
    "pluralName": "Red Berries",
    "type": "19",
    "craftStep": " "
  },
  {
    "id": "Blue Berry",
    "pluralName": "Blue Berries",
    "type": "20",
    "craftStep": " "
  },
  {
    "id": "Green Berry",
    "pluralName": "Green Berries",
    "type": "21",
    "craftStep": " "
  },
  {"id": "Feather", "pluralName": "Feathers", "type": "0", "craftStep": "64"},
  {"id": "Wolf Pet", "pluralName": "Wolf Pets", "type": "26", "craftStep": "2"},
  {
    "id": "Tiger Pet",
    "pluralName": "Tiger Pets",
    "type": "30",
    "craftStep": "84"
  },
  {
    "id": "Bear Pet",
    "pluralName": "Bear Pets",
    "type": "34",
    "craftStep": "99"
  },
  {"id": "Sead", "pluralName": "Seads", "type": "1", "craftStep": "2"},
  {
    "id": "Sharp Stone",
    "pluralName": "Sharp Stones",
    "type": "24",
    "craftStep": "3"
  },
  {"id": "Reed", "pluralName": "Reeds", "type": "0", "craftStep": "3"},
  {"id": "Fiber", "pluralName": "Fibers", "type": "0", "craftStep": "7"},
  {"id": "String", "pluralName": "String", "type": "0", "craftStep": "15"},
  {"id": "Rope", "pluralName": "Ropes", "type": "0", "craftStep": "31"},
  {
    "id": "Edible Plant Part",
    "pluralName": "Edible Plant Parts",
    "type": "2",
    "craftStep": "3"
  },
  {"id": "Stick", "pluralName": "Sticks", "type": "0", "craftStep": "2"},
  {
    "id": "Large Leaf",
    "pluralName": "Large Leaves",
    "type": "0",
    "craftStep": "2"
  },
  {"id": "Leaf Hat", "pluralName": "Leaf Hats", "type": "10", "craftStep": "3"},
  {
    "id": "Leaf Sandels",
    "pluralName": "Leaf Sandels",
    "type": "12",
    "craftStep": "4"
  },
  {
    "id": "Grass Skirt",
    "pluralName": "Grass Skirts",
    "type": "11",
    "craftStep": "5"
  },
  {
    "id": "Stone Knife",
    "pluralName": "Stone Knives",
    "type": "29",
    "craftStep": "6"
  },
  {
    "id": "Stone Axe",
    "pluralName": "Stone Axes",
    "type": "47",
    "craftStep": "3"
  },
  {
    "id": "Primative Mace",
    "pluralName": "Primative Mace",
    "type": "22",
    "craftStep": "4"
  },
  {"id": "Log", "pluralName": "Logs", "type": "0", "craftStep": "5"},
  {
    "id": "Stick with String",
    "pluralName": "Sticks with Strings",
    "type": "0",
    "craftStep": "18"
  },
  {
    "id": "Stick with String and Feather",
    "pluralName": "Sticks with Strings and Feathers",
    "type": "0",
    "craftStep": "83"
  },
  {"id": "Pole", "pluralName": "Poles", "type": "23", "craftStep": "11"},
  {
    "id": "Stone Spear",
    "pluralName": "Stone Spears",
    "type": "31",
    "craftStep": "15"
  },
  {
    "id": "Crafted Stone Spear",
    "pluralName": "Crafted Stone Spears",
    "type": "48",
    "craftStep": "18"
  },
  {
    "id": "Fishing Pole",
    "pluralName": "Fishing Poles",
    "type": "7",
    "craftStep": "27"
  },
  {
    "id": "Fly Fishing Pole",
    "pluralName": "Fly Fishing Poles",
    "type": "8",
    "craftStep": null
  },
  {"id": "Net", "pluralName": "Nets", "type": "9", "craftStep": "63"},
  {
    "id": "Leaf Loincloth",
    "pluralName": "Leaf Loincloths",
    "type": "11",
    "craftStep": "18"
  },
  {
    "id": "Partial Dream Catcher",
    "pluralName": "Partial Dream Catchers",
    "type": "0",
    "craftStep": "33"
  },
  {
    "id": "Dream Catcher",
    "pluralName": "Dream Catchers",
    "type": "0",
    "craftStep": "98"
  },
  {
    "id": "Feathered Hat",
    "pluralName": "Feathered hats",
    "type": "13",
    "craftStep": "68"
  },
  {
    "id": "Leather Shoes",
    "pluralName": "Leather Shoes",
    "type": "37",
    "craftStep": "5"
  },
  {
    "id": "Leather Jacket",
    "pluralName": "Leather Jackets",
    "type": "39",
    "craftStep": "19"
  },
  {
    "id": "Leather Helmut",
    "pluralName": "Leather Helmuts",
    "type": "35",
    "craftStep": "7"
  },
  {
    "id": "Wooden Sandels",
    "pluralName": "Wooden Sandels",
    "type": "37",
    "craftStep": "6"
  },
  {
    "id": "Wooden Shoes",
    "pluralName": "Wooden Shoes",
    "type": "40",
    "craftStep": "9"
  },
  {
    "id": "Wooden Brestplate",
    "pluralName": "Wooden Brestplates",
    "type": "42",
    "craftStep": "21"
  },
  {
    "id": "Powder",
    "pluralName": "Powders",
    "type": "0",
  }
];

List<Map<String, String>> itemTypesData = [
  {
    "id": "0",
    "name": "regular",
    "wearPosition": "0",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "1",
    "name": "food_1",
    "wearPosition": "0",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "2",
    "name": "food_2",
    "wearPosition": "0",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "3",
    "name": "food_3",
    "wearPosition": "0",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "4",
    "name": "tool_1",
    "wearPosition": "0",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "5",
    "name": "tool_2",
    "wearPosition": "0",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "6",
    "name": "tool_3",
    "wearPosition": "0",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "7",
    "name": "fishing_1",
    "wearPosition": "0",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "8",
    "name": "fishing_2",
    "wearPosition": "0",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "9",
    "name": "fishing_3",
    "wearPosition": "0",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "10",
    "name": "head_decoration_1",
    "wearPosition": "1",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "11",
    "name": "torso_decoration_1",
    "wearPosition": "2",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "12",
    "name": "feet_decoration_1",
    "wearPosition": "3",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "13",
    "name": "head_decoration_2",
    "wearPosition": "1",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "14",
    "name": "torso_decoration_2",
    "wearPosition": "2",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "15",
    "name": "feet_decoration_2",
    "wearPosition": "3",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "16",
    "name": "head_decoration_3",
    "wearPosition": "1",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "17",
    "name": "feet_decoration_3",
    "wearPosition": "2",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "18",
    "name": "torso_decoration_3",
    "wearPosition": "3",
    "damage": "0",
    "armor": "0"
  },
  {"id": "19", "name": "red", "wearPosition": "0", "damage": "0", "armor": "0"},
  {
    "id": "20",
    "name": "blue",
    "wearPosition": "0",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "21",
    "name": "green",
    "wearPosition": "0",
    "damage": "0",
    "armor": "0"
  },
  {
    "id": "22",
    "name": "weapon_1",
    "wearPosition": "0",
    "damage": "1",
    "armor": "0"
  },
  {
    "id": "23",
    "name": "tool_1_weapon_1",
    "wearPosition": "0",
    "damage": "1",
    "armor": "0"
  },
  {
    "id": "24",
    "name": "tool_2_weapon_1",
    "wearPosition": "0",
    "damage": "1",
    "armor": "0"
  },
  {
    "id": "25",
    "name": "axe_1_weapon_1",
    "wearPosition": "0",
    "damage": "1",
    "armor": "0"
  },
  {
    "id": "26",
    "name": "pet_weapon_1",
    "wearPosition": "4",
    "damage": "1",
    "armor": "0"
  },
  {
    "id": "27",
    "name": "weapon_2",
    "wearPosition": "0",
    "damage": "2",
    "armor": "0"
  },
  {
    "id": "28",
    "name": "tool_1_weapon_2",
    "wearPosition": "0",
    "damage": "2",
    "armor": "0"
  },
  {
    "id": "29",
    "name": "tool_2_weapon_2",
    "wearPosition": "0",
    "damage": "2",
    "armor": "0"
  },
  {
    "id": "30",
    "name": "pet_weapon_2",
    "wearPosition": "4",
    "damage": "2",
    "armor": "0"
  },
  {
    "id": "31",
    "name": "weapon_3",
    "wearPosition": "0",
    "damage": "3",
    "armor": "0"
  },
  {
    "id": "32",
    "name": "tool_1_weapon_3",
    "wearPosition": "0",
    "damage": "3",
    "armor": "0"
  },
  {
    "id": "33",
    "name": "tool_2_weapon_3",
    "wearPosition": "0",
    "damage": "3",
    "armor": "0"
  },
  {
    "id": "34",
    "name": "pet_weapon_3",
    "wearPosition": "4",
    "damage": "3",
    "armor": "0"
  },
  {
    "id": "35",
    "name": "head_armor_1",
    "wearPosition": "1",
    "damage": "0",
    "armor": "1"
  },
  {
    "id": "36",
    "name": "torso_armor_1",
    "wearPosition": "2",
    "damage": "0",
    "armor": "1"
  },
  {
    "id": "37",
    "name": "feet_armor_1",
    "wearPosition": "3",
    "damage": "0",
    "armor": "1"
  },
  {
    "id": "38",
    "name": "head_armor_2",
    "wearPosition": "1",
    "damage": "0",
    "armor": "2"
  },
  {
    "id": "39",
    "name": "torso_armor_2",
    "wearPosition": "2",
    "damage": "0",
    "armor": "2"
  },
  {
    "id": "40",
    "name": "feet_armor_2",
    "wearPosition": "3",
    "damage": "0",
    "armor": "2"
  },
  {
    "id": "41",
    "name": "head_armor_3",
    "wearPosition": "1",
    "damage": "0",
    "armor": "3"
  },
  {
    "id": "42",
    "name": "torso_armor_3",
    "wearPosition": "2",
    "damage": "0",
    "armor": "3"
  },
  {
    "id": "43",
    "name": "feet_armor_3",
    "wearPosition": "3",
    "damage": "0",
    "armor": "3"
  },
  {
    "id": "44",
    "name": "head_armor_4",
    "wearPosition": "1",
    "damage": "0",
    "armor": "4"
  },
  {
    "id": "45",
    "name": "torso_armor_4",
    "wearPosition": "2",
    "damage": "0",
    "armor": "4"
  },
  {
    "id": "46",
    "name": "feet_armor_4",
    "wearPosition": "3",
    "damage": "0",
    "armor": "4"
  },
  {
    "id": "47",
    "name": "axe_1_weapon_2",
    "wearPosition": "0",
    "damage": "2",
    "armor": "0"
  },
  {
    "id": "48",
    "name": "weapon_4",
    "wearPosition": "0",
    "damage": "4",
    "armor": "0"
  }
];

List<Map<String, String>> recipesData = [
  {
    "id": "1",
    "carried": "tool_1",
    "target": "Forrest Plant",
    "itemCreated": "Sead",
    "carriedConsumed": "1",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "2",
    "carried": "Stone",
    "target": "Stone",
    "itemCreated": "Sharp Stone",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "3",
    "carried": "tool_1",
    "target": "Wetlands Plant",
    "itemCreated": "Reed",
    "carriedConsumed": "1",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "4",
    "carried": "tool_1",
    "target": "Reed",
    "itemCreated": "Fiber",
    "carriedConsumed": "1",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "5",
    "carried": "Fiber",
    "target": "Fiber",
    "itemCreated": "String",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "6",
    "carried": "String",
    "target": "String",
    "itemCreated": "Rope",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "7",
    "carried": "tool_1",
    "target": "Sead",
    "itemCreated": "Edible Plant Parts",
    "carriedConsumed": "1",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "8",
    "carried": "Forrest Plant",
    "target": "Forrest Plant",
    "itemCreated": "Stick",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "9",
    "carried": "Wetlands Plant",
    "target": "Wetlands Plant",
    "itemCreated": "Large Leaf",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "10",
    "carried": "Large Leaf",
    "target": "Large Leaf",
    "itemCreated": "Leaf Hat",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "11",
    "carried": "tool_1",
    "target": "Large Leaf",
    "itemCreated": "Leaf Sandels",
    "carriedConsumed": "1",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "12",
    "carried": "Reed",
    "target": "Reed",
    "itemCreated": "Grass Skirt",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "13",
    "carried": "String",
    "target": "Sharp Stone",
    "itemCreated": "Stone Knife",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "14",
    "carried": "Sharp Stone",
    "target": "String",
    "itemCreated": "Stone Knife",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "15",
    "carried": "Sharp Stone",
    "target": "Stick",
    "itemCreated": "Stone Axe",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "16",
    "carried": "Stick",
    "target": "Sharp Stone",
    "itemCreated": "Stone Axe",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "17",
    "carried": "Stone",
    "target": "Stick",
    "itemCreated": "Primative Mace",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "18",
    "carried": "Stick",
    "target": "Stone",
    "itemCreated": "Primative Mace",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "19",
    "carried": "Stick",
    "target": "Stick",
    "itemCreated": "Log",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "20",
    "carried": "String",
    "target": "Stick",
    "itemCreated": "Stick with String",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "21",
    "carried": "Stick",
    "target": "String",
    "itemCreated": "Stick with String",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "22",
    "carried": "Stick with String",
    "target": "Feather",
    "itemCreated": "Stick with String and Feather",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "23",
    "carried": "Feather",
    "target": "Stick with String",
    "itemCreated": "Stick with String and Feather",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "24",
    "carried": "Log",
    "target": "Log",
    "itemCreated": "Pole",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "25",
    "carried": "Sharp Stone",
    "target": "Pole",
    "itemCreated": "Stone Spear",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "26",
    "carried": "Pole",
    "target": "Sharp Stone",
    "itemCreated": "Stone Spear",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "27",
    "carried": "Stone Knife",
    "target": "Pole",
    "itemCreated": "Crafted Stone Spear",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "28",
    "carried": "Pole",
    "target": "Stone Knife",
    "itemCreated": "Crafted Stone Spear",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "29",
    "carried": "Pole",
    "target": "String",
    "itemCreated": "Fishing Pole",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "30",
    "carried": "String",
    "target": "Pole",
    "itemCreated": "Fishing Pole",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "31",
    "carried": "Fishing Pole",
    "target": "Feather",
    "itemCreated": "Fly Fishing Pole",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "32",
    "carried": "Feather",
    "target": "Fishing Pole",
    "itemCreated": "Fly Fishing Pole",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "33",
    "carried": "Rope",
    "target": "Rope",
    "itemCreated": "Net",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "34",
    "carried": "Large Leaf",
    "target": "String",
    "itemCreated": "Leaf Loincloth",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "35",
    "carried": "String",
    "target": "Large Leaf",
    "itemCreated": "Leaf Loincloth",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "36",
    "carried": "Stick with String",
    "target": "Feather",
    "itemCreated": "Partial Dream Catcher",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "37",
    "carried": "Feather",
    "target": "Stick with String",
    "itemCreated": "Partial Dream Catcher",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "38",
    "carried": "axe_1",
    "target": "Log",
    "itemCreated": "Stick",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "39",
    "carried": "Feather",
    "target": "Leaf Hat",
    "itemCreated": "Feathered Hat",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "40",
    "carried": "tool_1",
    "target": "Leather",
    "itemCreated": "Leather Shoes",
    "carriedConsumed": "1",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "41",
    "carried": "Leather",
    "target": "String",
    "itemCreated": "Leather Jacket",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "42",
    "carried": "String",
    "target": "Leather",
    "itemCreated": "Leather Jacket",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "43",
    "carried": "Leather",
    "target": "Leather",
    "itemCreated": "Leather Helmut",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "44",
    "carried": "tool_2",
    "target": "Stick",
    "itemCreated": "Wooden Sandels",
    "carriedConsumed": "1",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "45",
    "carried": "tool_2",
    "target": "Log",
    "itemCreated": "Wooden Shoes",
    "carriedConsumed": "1",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "46",
    "carried": "String",
    "target": "Log",
    "itemCreated": "Wooden Brestplate",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "47",
    "carried": "Log",
    "target": "String",
    "itemCreated": "Wooden Brestplate",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "48",
    "carried": "",
    "target": "Stone",
    "itemCreated": "Powder",
    "carriedConsumed": "0",
    "targetConsumed": "1",
    "message": null
  },
  {
    "id": "49",
    "carried": "",
    "target": "Wetlands Plant",
    "itemCreated": "",
    "carriedConsumed": "0",
    "targetConsumed": "0",
    "message":
        "The person does something with wetlands plant but nothing happened."
  }
];

//List<Map<String, String>> sentenceStructureData = [
//  {
//    "personName":"person",
//    "equippedItem":"wearing",
//    "handledItem":"holding",
//    "followingPet":"followed by",
//    "verb":"stands",
//    "hasEncounter":"in",
//    "hasEncounterCont":"with",
//    "noEncounter":"alone in",
//    "noEncounterCont":"",
//    "worldName":"sparse world",
//    "droppedItems":"There",
//    "droppedItemsCont":"on the ground",
//    "deadState":"The person is dead."
//  }
//];

List<Map<String, String>> personStateData = [
  {"energy": "0", "status": "dead"},
  {"energy": "1", "status": "wounded"},
  {"energy": "2", "status": "hungry"},
  {"energy": "3", "status": "thirsty"},
  {"energy": "4", "status": "tired"},
  {"energy": "5", "status": null}
];

List<Map<String, String>> fightDamageMessageData = [
  {
    "id": "0",
    "personDamage": "0",
    "guardDamage": "0",
    "guard": "Jackel",
    "message": "Both person and jackel has no damage"
  },
  {
    "id": "1",
    "personDamage": "1",
    "guardDamage": "0",
    "guard": "Jackel",
    "message":
        "Person made a little damage to jackel. Jackel did not make any damage"
  },
  {
    "id": "2",
    "personDamage": "2",
    "guardDamage": "0",
    "guard": "Jackel",
    "message":
        "Person made a mild damage to jackel. Jackel did not make any damage"
  },
  {
    "id": "3",
    "personDamage": "3",
    "guardDamage": "0",
    "guard": "Jackel",
    "message":
        "Person made a serious damage to jackel. Jackel did not make any damage"
  },
  {
    "id": "4",
    "personDamage": "4",
    "guardDamage": "0",
    "guard": "Jackel",
    "message":
        "Person made a critical damage to jackel. Jackel did not make any damage"
  },
  {
    "id": "5",
    "personDamage": "0",
    "guardDamage": "1",
    "guard": "Jackel",
    "message":
        "Person did not deal a a damage to jackel. Jackel scratched the person."
  },
  {
    "id": "6",
    "personDamage": "1",
    "guardDamage": "1",
    "guard": "Jackel",
    "message": "Both jackel and person take a mild damage from each other."
  },
  {
    "id": "7",
    "personDamage": "2",
    "guardDamage": "1",
    "guard": "Jackel",
    "message":
        "Person made a mild damage to jackel. Jackel scratched the person."
  },
  {
    "id": "8",
    "personDamage": "3",
    "guardDamage": "1",
    "guard": "Jackel",
    "message":
        "Person made a serious damage to jackel. Jackel scratched the person."
  },
  {
    "id": "9",
    "personDamage": "4",
    "guardDamage": "1",
    "guard": "Jackel",
    "message":
        "Person made a critical damage to jackel. Jackel scratched the person."
  },
  {
    "id": "10",
    "personDamage": "0",
    "guardDamage": "2",
    "guard": "Jackel",
    "message": null
  },
  {
    "id": "11",
    "personDamage": "1",
    "guardDamage": "2",
    "guard": "Jackel",
    "message": null
  },
  {
    "id": "12",
    "personDamage": "2",
    "guardDamage": "2",
    "guard": "Jackel",
    "message": null
  },
  {
    "id": "13",
    "personDamage": "3",
    "guardDamage": "2",
    "guard": "Jackel",
    "message": null
  },
  {
    "id": "14",
    "personDamage": "4",
    "guardDamage": "2",
    "guard": "Jackel",
    "message": null
  },
  {
    "id": "15",
    "personDamage": "0",
    "guardDamage": "3",
    "guard": "Jackel",
    "message": null
  },
  {
    "id": "16",
    "personDamage": "1",
    "guardDamage": "3",
    "guard": "Jackel",
    "message": null
  },
  {
    "id": "17",
    "personDamage": "2",
    "guardDamage": "3",
    "guard": "Jackel",
    "message": null
  },
  {
    "id": "18",
    "personDamage": "3",
    "guardDamage": "3",
    "guard": "Jackel",
    "message": null
  },
  {
    "id": "19",
    "personDamage": "4",
    "guardDamage": "3",
    "guard": "Jackel",
    "message": null
  },
  {
    "id": "20",
    "personDamage": "0",
    "guardDamage": "4",
    "guard": "Jackel",
    "message": null
  },
  {
    "id": "21",
    "personDamage": "1",
    "guardDamage": "4",
    "guard": "Jackel",
    "message": null
  },
  {
    "id": "22",
    "personDamage": "2",
    "guardDamage": "4",
    "guard": "Jackel",
    "message": null
  },
  {
    "id": "23",
    "personDamage": "3",
    "guardDamage": "4",
    "guard": "Jackel",
    "message": null
  },
  {
    "id": "24",
    "personDamage": "4",
    "guardDamage": "4",
    "guard": "Jackel",
    "message": null
  }
];

List<Map<String, String>> interfaceWordsData = [
  {"id": "title", "text": "Sparse", "comments (not included on api)": null},
  {
    "id": "log_box_name",
    "text": "Input Log",
    "comments (not included on api)": null
  },
  {
    "id": "clear_log",
    "text": "Clear Logs",
    "comments (not included on api)": null
  },
  {
    "id": "previously",
    "text": "previously...",
    "comments (not included on api)": null
  },
  {
    "id": "button_hide_log",
    "text": "Hide Log Button",
    "comments (not included on api)": null
  },
  {
    "id": "button_show_log",
    "text": "Show Log Button",
    "comments (not included on api)": null
  },
  {
    "id": "button_reset",
    "text": "Reset Game",
    "comments (not included on api)": null
  },
  {
    "id": "world_action1",
    "text": "Explore",
    "comments (not included on api)": null
  },
  {
    "id": "handled_item_action1",
    "text": "Drop",
    "comments (not included on api)": null
  },
  {
    "id": "encounter_action1",
    "text": "Encounter",
    "comments (not included on api)": null
  },
  {
    "id": "dropped_item_action1",
    "text": "Equip",
    "comments (not included on api)": null
  },
  {
    "id": "dropped_item_action2",
    "text": "Craft",
    "comments (not included on api)": null
  },
  {
    "id": "guard_action1",
    "text": "Tame",
    "comments (not included on api)": null
  },
  {
    "id": "guard_action2",
    "text": "Fight",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_explore_1",
    "text": "The person explores far and wide until he finds ",
    "comments (not included on api)": "Format: 1 (ENCOUNTER) 2 3(GUARD)4"
  },
  {
    "id": "announcement_explore_2",
    "text": " on the world",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_explore_3",
    "text": " guarded by ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_explore_4",
    "text": ".",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_explore_all_explored",
    "text": "The person explored the whole sparse world.",
    "comments (not included on api)": "if all encounters are explored"
  },
  {
    "id": "announcement_harvest_found_1",
    "text": "The person harvests from the ",
    "comments (not included on api)": "Format: 1 (ENCOUNTER) 2 (ITEM) 3"
  },
  {
    "id": "announcement_harvest_found_2",
    "text": " and finds ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_harvest_found_3",
    "text": " luckily.",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_harvest_no_item_1",
    "text": "The person harvests from the ",
    "comments (not included on api)": "Format: 1 (ENCOUNTER) 2"
  },
  {
    "id": "announcement_harvest_no_item_2",
    "text": " and no item found. ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_harvest_cont_replaced_1",
    "text": "The ",
    "comments (not included on api)":
        "Format: 1 (ENCOUNTER) 2 (ENCOUNTER CREATED) 3"
  },
  {
    "id": "announcement_harvest_cont_replaced_2",
    "text": " was replaced by ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_harvest_cont_replaced_3",
    "text": " encounter.",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_harvest_cont_used_1",
    "text": "The ",
    "comments (not included on api)": "Format: 1 (ENCOUNTER) 2"
  },
  {
    "id": "announcement_harvest_cont_used_2",
    "text": " is all used up.",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_harvest_blocked_1",
    "text": "Harvest on ",
    "comments (not included on api)": "Format: 1 (ENCOUNTER) 2"
  },
  {
    "id": "announcement_harvest_blocked_2",
    "text": " is not allowed. Deal with the guard first.",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_drop_item_1",
    "text": "The person takes off and drops ",
    "comments (not included on api)": "Format: 1 (EQUIPPED ITEM) 2"
  },
  {
    "id": "announcement_drop_item_2",
    "text": ".",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_equip_new_1",
    "text": "The person picks up and puts on ",
    "comments (not included on api)": "Format: 1 (TARGET ITEM) 2"
  },
  {
    "id": "announcement_equip_new_2",
    "text": ".",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_equip_swap_1",
    "text": "The person swaps the ",
    "comments (not included on api)":
        "Format: 1 (HANDLED ITEM) 2 (TARGET ITEM) 3"
  },
  {
    "id": "announcement_equip_swap_2",
    "text": " with ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_equip_swap_3",
    "text": ".",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_craft_handled_success_1",
    "text": "The person crafts ",
    "comments (not included on api)":
        "Format: 1 (TARGET ITEM) 2 (HANDLED ITEM) 3 (CREATED ITEM) 4"
  },
  {
    "id": "announcement_craft_handled_success_2",
    "text": " with ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_craft_handled_success_3",
    "text": " and creates ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_craft_handled_success_4",
    "text": ".",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_craft_handled_failed_1",
    "text": "The person crafts ",
    "comments (not included on api)":
        "Format 1 (TARGET ITEM) 2 (HANDLED ITEM) 3"
  },
  {
    "id": "announcement_craft_handled_failed_2",
    "text": " with ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_craft_handled_failed_3",
    "text": " but no possible result for this combination.",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_craft_barehanded_success_1",
    "text": "The person crafts ",
    "comments (not included on api)":
        "Format : 1 (TARGET ITEM) 2 (REATED ITEM) 3"
  },
  {
    "id": "announcement_craft_barehanded_success_2",
    "text": " barehandedly and creates ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_craft_barehanded_success_3",
    "text": ".",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_craft_barehanded_failed_1",
    "text": "The person crafts ",
    "comments (not included on api)": "Format: 1 (TARGET ITEM) 2"
  },
  {
    "id": "announcement_craft_barehanded_failed_2",
    "text": " barehandedly but no possible result for this combination.",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_tame_success_1",
    "text": "The person tamed the ",
    "comments (not included on api)": "Format: 1 (GUARD) 2"
  },
  {
    "id": "announcement_guard_tame_success_2",
    "text": ".",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_tame_handled_failed_1",
    "text": "The person attempts to tame the ",
    "comments (not included on api)": "Format: 1 (GUARD) 2 (HANDLED ITEM) 3 "
  },
  {
    "id": "announcement_guard_tame_handled_failed_2",
    "text": " but he is holding a ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_tame_handled_failed_3",
    "text": " which only infuriates it and attacked the person.",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_tame_barehanded_failed_1",
    "text": "The person attempts to tame the ",
    "comments (not included on api)": "Format: 1 (GUARD) 2"
  },
  {
    "id": "announcement_guard_tame_barehanded_failed_2",
    "text":
        " but he is barehanded which only infuriates it and attacked the person.",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_person_tame_damage_1",
    "text": " Person received ",
    "comments (not included on api)":
        "Format: 1 (DAMAGE TO PERSON) 2 (PERSON ENERGY) 3 "
  },
  {
    "id": "announcement_guard_person_tame_damage_2",
    "text": " damage. Person Energy: ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_person_tame_damage_3",
    "text": ".",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_fight_start_1",
    "text": "The person and the ",
    "comments (not included on api)": "Format: 1 (GUARD) 2 (ENCOUNTER) 3 "
  },
  {
    "id": "announcement_guard_fight_start_2",
    "text": " guarding the ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_fight_start_3",
    "text": " fights.",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_fight_damages_1",
    "text": "Person received ",
    "comments (not included on api)":
        "Format: 1 (DAMAGE TO PERSON) 2 (GUARD) 3 (DAMAGE TO GUARD) 4"
  },
  {
    "id": "announcement_guard_fight_damages_2",
    "text": " damage.",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_fight_damages_3",
    "text": " received ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_fight_damages_4",
    "text": " damage.",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_fight_energy_1",
    "text": "Person Energy: ",
    "comments (not included on api)":
        "Format: 1 (PERSON ENERGY) 2 (GUARD) 3 (GUARD ENERGY) 4"
  },
  {
    "id": "announcement_guard_fight_energy_2",
    "text": ".",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_fight_energy_3",
    "text": " Energy: ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_fight_energy_4",
    "text": ".",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_died_1",
    "text": "The person killed the ",
    "comments (not included on api)": "Format: 1 (GUARD) 2 (ENCOUNTER) 3 "
  },
  {
    "id": "announcement_guard_died_2",
    "text": " guarding the ",
    "comments (not included on api)": null
  },
  {
    "id": "announcement_guard_died_3",
    "text": ".",
    "comments (not included on api)": null
  },
  {
    "id": "structure_person_name",
    "text": "warrior",
    "comments (not included on api)": null
  },
  {
    "id": "structure_equipped_item",
    "text": "wearing",
    "comments (not included on api)": null
  },
  {
    "id": "structure_handled_item",
    "text": "holding",
    "comments (not included on api)": null
  },
  {
    "id": "structure_following_pet",
    "text": "followed by",
    "comments (not included on api)": null
  },
  {
    "id": "structure_verb",
    "text": "stands",
    "comments (not included on api)": null
  },
  {
    "id": "structure_has_encounter",
    "text": "in",
    "comments (not included on api)": null
  },
  {
    "id": "structure_has_encounter_2",
    "text": "with",
    "comments (not included on api)": null
  },
  {
    "id": "structure_no_encounter",
    "text": "alone in",
    "comments (not included on api)": null
  },
  {
    "id": "structure_no_encounter_2",
    "text": null,
    "comments (not included on api)": null
  },
  {
    "id": "structure_world_name",
    "text": "sparse world",
    "comments (not included on api)": null
  },
  {
    "id": "structure_dropped_items",
    "text": "There",
    "comments (not included on api)": null
  },
  {
    "id": "structure_dropped_items_2",
    "text": "on the ground",
    "comments (not included on api)": null
  },
  {
    "id": "structure_dead_state",
    "text": "The person is dead.",
    "comments (not included on api)": null
  }
];
