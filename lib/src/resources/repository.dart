import 'package:sparse/src/models/encounter_list_item.dart';
import 'package:sparse/src/models/encounter.dart';
import 'package:sparse/src/models/encounter_table_item.dart';
import 'package:sparse/src/models/encounter_trigger.dart';
import 'package:sparse/src/models/fight_damage_message.dart';
import 'package:sparse/src/models/guard_table_item.dart';
import 'package:sparse/src/models/guard.dart';
import 'package:sparse/src/models/encounter_result.dart';
import 'package:sparse/src/models/interface_words.dart';
import 'package:sparse/src/models/item.dart';
import 'package:sparse/src/models/person.dart';
import 'package:sparse/src/models/person_state.dart';
import 'package:sparse/src/models/result_items.dart';
import 'package:sparse/src/models/item_type.dart';
import 'package:sparse/src/models/recipe.dart';
import 'package:sparse/src/resources/api_provider.dart';

class Repository {
  final _apiProvider = ApiProvider();
  Future<Person> getPersonData() => _apiProvider.getPersonData();
  Future<List<EncounterListItem>> getEncounterListData() =>
      _apiProvider.getEncounterListData();
  Future<List<EncounterTableItem>> getEncounterTableData() =>
      _apiProvider.getEncounterTableData();
  Future<List<Encounter>> getEncountersData() =>
      _apiProvider.getEncountersData();
  Future<List<GuardTableItem>> getGuardTableData() =>
      _apiProvider.getGuardTableData();
  Future<List<Guard>> getGuardData() => _apiProvider.getGuardData();
  Future<List<EncounterResult>> getEncounterResultsData() =>
      _apiProvider.getEncounterResultsData();
  Future<List<ResultItems>> getResultItemsData() =>
      _apiProvider.getResultItemsData();
  Future<List<Item>> getItemsData() => _apiProvider.getItemsData();
  Future<List<ItemType>> getItemTypesData() => _apiProvider.getItemTypesData();
  Future<List<Recipe>> getRecipesData() => _apiProvider.getRecipesData();
//  Future<SentenceStructure> getSentenceStructureData() => _apiProvider.getSentenceStructureData();
  Future<List<PersonState>> getPersonStateData() =>
      _apiProvider.getPersonStateData();
  Future<List<FightDamageMessage>> getFightDamageMessageData() =>
      _apiProvider.getFightDamageMessageData();
  Future<List<InterfaceWords>> getInterfaceWords() =>
      _apiProvider.getInterfaceWords();
  Future<List<EncounterTrigger>> getEncounterTriggers() =>
      _apiProvider.getEncounterTriggers();
}
