import 'package:flutter/material.dart';
import 'package:sparse/src/models/encounter_list_item.dart';
import 'package:sparse/src/models/encounter.dart';
import 'package:sparse/src/models/encounter_trigger.dart';
import 'package:sparse/src/models/guard_table_item.dart';
import 'package:sparse/src/models/guard.dart';
import 'package:sparse/src/models/encounter_result.dart';
import 'package:sparse/src/models/interface_words.dart';
import 'package:sparse/src/models/item.dart';
import 'package:sparse/src/models/person_state.dart';
import 'package:sparse/src/models/result_items.dart';
import 'package:sparse/src/models/item_type.dart';
import 'package:number_to_words_spelling/number_to_words_spelling.dart';

class Utils {
  static bool startsWithVowel(String text) {
    String _text = text.toLowerCase();
    bool isVowel = _text.startsWith("a") ||
        _text.startsWith("e") ||
        _text.startsWith("i") ||
        _text.startsWith("o") ||
        _text.startsWith("u");
    return isVowel;
  }

  static String numberToWord(int number) {
    return NumberWordsSpelling.toWord('$number', 'en_US');
  }

  static List<EncounterListItem> getEncounterListByTableId(
      List<EncounterListItem> encounterList, int tableId) {
    if (tableId != null) {
      return encounterList
          .where((encounterList) => encounterList.encounterTableId == tableId)
          .toList();
    }
    return null;
  }

  static Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  static List<EncounterTrigger> getEncounterTriggersByEncounterId(
      List<EncounterTrigger> encounterTriggerList, String encounterId) {
    if (encounterId != null) {
      return encounterTriggerList
          .where((triggerList) => triggerList.encounterId == encounterId)
          .toList();
    }
    return null;
  }

  static Encounter getEncounterById(List<Encounter> encounters, String id) {
    if (id != null) {
      try {
        Encounter encounter =
            encounters.singleWhere((encounter) => encounter.id == id);
        return Encounter(
          id: encounter.id,
          uses: encounter.uses,
          pluralName: encounter.pluralName,
          message: encounter.message,
          encounterReplacement: encounter.encounterReplacement,
          explored: encounter.explored,
        );
      } catch (e) {
        print("no $id on encounter list");
        return null;
      }
    }
    return null;
  }

  static List<GuardTableItem> getGuardTableByTableId(
      List<GuardTableItem> guardTable, int tableId) {
    if (tableId != null) {
      return guardTable
          .where((encounterList) => encounterList.tableId == tableId)
          .toList();
    }
    return null;
  }

  static Guard getGuardbyId(List<Guard> guards, String id) {
    if (id != null) {
      try {
        Guard guard = guards.singleWhere((encounter) => encounter.id == id);
        return Guard(
            id: guard.id,
            armor: guard.armor,
            masterItemId: guard.masterItemId,
            damage: guard.damage,
            energy: guard.energy,
            itemId: guard.itemId,
            color: guard.color,
            message: guard.message);
      } catch (e) {
        print("no $id on guard list");
        return null;
      }
    }
    return null;
  }

  static List<EncounterResult> getEncounterResultsByGroupId(
      List<EncounterResult> encounterResults, int groupId) {
    if (groupId != null) {
      return encounterResults
          .where((encounterResult) => encounterResult.groupId == groupId)
          .toList();
    }
    return null;
  }

  static List<ResultItems> getResultItemsById(
      List<ResultItems> resultItems, int resultItemsId) {
    if (resultItemsId != null) {
      return resultItems
          .where((encounterResult) =>
              encounterResult.resultItemsId == resultItemsId)
          .toList();
    }
    return null;
  }

  static Item getItemById(List<Item> items, String id) {
    try {
      Item item = items.singleWhere((item) => item.id == id);
      return Item(
          id: item.id,
          pluralName: item.pluralName,
          type: item.type,
          quantity: 1);
    } catch (e) {
      print("no $id on item list");
      return null;
    }
  }

  static ItemType getItemTypeById(List<ItemType> itemTypes, int id) {
    if (id != null) {
      return itemTypes.singleWhere((itemType) => itemType.id == id);
    }
    return null;
  }
}

class Sheets {
  static const String BASE_URL =
      "https://sheet.best/api/sheets/7ceeef08-afc4-4824-8ea7-8f0a42164d85/tabs/";
  static const String TAB_ENCOUNTER_TABLE = "Encounter Table";
  static const String TAB_ENCOUNTER_LIST = "Encounter List";
  static const String TAB_ENCOUNTERS = "Encounters";
  static const String TAB_RESULTS = "Results";
  static const String TAB_RESULT_ITEMS = "Result Items";
  static const String TAB_GUARD_TABLES = "Guard Tables";
  static const String TAB_GUARDS = "Guards";
  static const String TAB_ITEMS = "Items";
  static const String TAB_ITEM_TYPES = "Item Types";
  static const String TAB_RECIPES = "Recipes";
  static const String TAB_PERSON = "Person";
  static const String TAB_PERSON_STATE = "Person State";
  static const String TAB_FIGHT_DAMAGE_MESSAGE = "Fight Damage Message";
//  static const String TAB_SENTENCE_STRUCTURE = "Sentence Structure";
  static const String TAB_INTERFACE_WORDS = "Interface Words";
  static const String TAB_ENCOUNTER_TRIGGERS = "Encounter Triggers";
}

class Elements {
  static const String SPARSE_WORLD = "sparse_world";
  static const String ENCOUNTER = "encounter";
  static const String HANDLED_ITEM = "handled_item";
  static const String DROPPED_ITEM = "dropped_item";
  static const String GUARD = "guard";
  static const String GUARD_TAME = "guard_tame";
  static const String GUARD_FIGHT = "guard_fight";
  static const String DROPPED_ITEM_EQUIP = "dropped_item_equip";
  static const String DROPPED_ITEM_CRAFT = "dropped_item_craft";
}

class Configs {
  static const int CROSSFADE_DURATION = 3000;

  ///in miliseconds
  static const int DEBUG_TYPING_SPEED = 3;
  static const int TYPING_SPEED = 30; //30 default
  static const String FONT_FAMILY = "Proxima Nova";
  static const String SCRIPT_FONT_FAMILY = "Madina";

  static const TEXT_ANIMATION_FROM_STYLE = TextStyle(
      fontSize: 27, fontFamily: Configs.SCRIPT_FONT_FAMILY, height: 1.3);

  static const DEFAULT_TEXT_STYLE =
      TextStyle(fontSize: 18, fontFamily: Configs.FONT_FAMILY, height: 1.3);
}

class InterfaceKey {
  static const String TITLE = "title";
  static const String LOG_BOX_NAME = "log_box_name";
  static const String CLEAR_LOG = "clear_log";
  static const String PREVIOUSLY = "previously";
  static const String BUTTON_HIDE_LOG = "button_hide_log";
  static const String BUTTON_SHOW_LOG = "button_show_log";
  static const String BUTTON_RESET = "button_reset";
  static const String WORLD_ACTION1 = "world_action1";
  static const String HANDLED_ITEM_ACTION1 = "handled_item_action1";
  static const String ENCOUNTER_ACTION1 = "encounter_action1";
  static const String DROPPED_ITEM_ACTION1 = "dropped_item_action1";
  static const String DROPPED_ITEM_ACTION2 = "dropped_item_action2";
  static const String GUARD_ACTION1 = "guard_action1";
  static const String GUARD_ACTION2 = "guard_action2";
}

class WearPosition {
  static const int HAND = 0;
  static const int HEAD = 1;
  static const int TORSO = 2;
  static const int FEET = 3;
  static const int PET = 4;
}

class Globals {
  static final Globals _singleton = new Globals._internal();

  factory Globals() {
    return _singleton;
  }
  Globals._internal();

  //general variables
  List<PersonState> _personStateData;
  List<InterfaceWords> _interfaceWordsData;

  ///setters
  void setPersonStateData(List<PersonState> personStateData) {
    this._personStateData = personStateData;
  }

  void setInterfaceWordsData(List<InterfaceWords> interfaceWordsData) {
    this._interfaceWordsData = interfaceWordsData;
  }

  ///functions
  String personState(int energy) {
    String personStatus = _personStateData
        .singleWhere((personState) => personState.energy == energy)
        .status;
    return "${Utils.startsWithVowel("") ? "An" : "A"} $personStatus${personStatus == "" ? "" : " "}";
  }

  String interfaceWord(String id) {
    String data;
    try {
      data = _interfaceWordsData
          .singleWhere((interfaceWord) => interfaceWord.id == id)
          .text;
    } catch (e) {
      print("$id not found");
    }
    if (data == null) {
      print("Note: $id is null");
      data = "";
    }
    return data;
  }
}
