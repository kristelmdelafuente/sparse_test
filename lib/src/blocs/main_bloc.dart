import 'dart:async';

import 'package:chance/chance.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sparse/src/models/encounter_list_item.dart';
import 'package:sparse/src/models/encounter.dart';
import 'package:sparse/src/models/encounter_table_item.dart';
import 'package:sparse/src/models/encounter_trigger.dart';
import 'package:sparse/src/models/fight_damage_message.dart';
import 'package:sparse/src/models/guard_table_item.dart';
import 'package:sparse/src/models/guard.dart';
import 'package:sparse/src/models/interface_words.dart';
import 'package:sparse/src/models/item.dart';
import 'package:sparse/src/models/person.dart';
import 'package:sparse/src/models/person_state.dart';
import 'package:sparse/src/models/sparse_world.dart';
import 'package:sparse/src/models/encounter_result.dart';
import 'package:sparse/src/models/result_items.dart';
import 'package:sparse/src/models/item_type.dart';
import 'package:sparse/src/models/recipe.dart';
import 'package:sparse/src/resources/repository.dart';
import 'package:sparse/src/utility.dart';
import 'package:collection/collection.dart';

class MainBloc {
  final _repository = Repository();
  var _chance = Chance();
  SparseWorld _sparseWorldValue = SparseWorld();
  String _lastResult;
  Person _personData;
  int _maxHealth;
  List<PersonState> _personStateData;
  List<PersonState> get personStateData => _personStateData;
  List<InterfaceWords> _interfaceWordsData;
  List<InterfaceWords> get interfaceWordsData => _interfaceWordsData;
//  SentenceStructure _sentenceStructureData;
//  SentenceStructure get sentenceStructureData => _sentenceStructureData;
  List<EncounterListItem> _encounterList;
  List<EncounterTableItem> _encounterTable;
  List<FightDamageMessage> _fightDamageMessageData;
  List<Encounter> _encountersData;
  List<EncounterTrigger> _encounterTriggersData;
  List<GuardTableItem> _guardTableData;
  List<Guard> _guardsData;
  List<EncounterResult> _encounterResultsData;
  List<ResultItems> _resultItemsData;
  List<Item> _itemsData;
  List<Item> get itemsData => _itemsData;

  Timer _multiTapTimer;
  int _multiTapCount = 0;
  int get multiTapCount => multiTapCount;

  List<String> _storedHistoryTexts = [];

  List<ItemType> _itemTypesData;
  List<Recipe> _recipesData;

  final BehaviorSubject<String> _inputLogs = BehaviorSubject<String>();
  Sink<String> get inputLogsSink => _inputLogs.sink;
  Stream<String> get inputLogsStream => _inputLogs.stream;

  final BehaviorSubject<String> _currentResultLog = BehaviorSubject<String>();
  Sink<String> get currentResultLogSink => _currentResultLog.sink;
  Stream<String> get currentResultLogStream => _currentResultLog.stream;

  final BehaviorSubject<Person> _person = BehaviorSubject<Person>();
  Sink<Person> get personSink => _person.sink;
  Stream<Person> get personStream => _person.stream;

  final BehaviorSubject<SparseWorld> _sparseWorld =
      BehaviorSubject<SparseWorld>();
  Sink<SparseWorld> get sparseWorldSink => _sparseWorld.sink;
  Stream<SparseWorld> get sparseWorldStream => _sparseWorld.stream;

  ///all data load stream
  final BehaviorSubject<bool> _allDataLoaded = BehaviorSubject<bool>();
  Sink<bool> get allDataLoadedSink => _allDataLoaded.sink;
  Stream<bool> get allDataLoadedStream => _allDataLoaded.stream;

  final BehaviorSubject<bool> _coverAnimationDone = BehaviorSubject<bool>();
  Sink<bool> get coverAnimationDoneSink => _coverAnimationDone.sink;
  Stream<bool> get coverAnimationDoneStream => _coverAnimationDone.stream;

  final BehaviorSubject<bool> _worldStateTypingAnimationDone =
      BehaviorSubject<bool>();
  Sink<bool> get worldStatetypingAnimationDoneSink =>
      _worldStateTypingAnimationDone.sink;
  Stream<bool> get worldStatetypingAnimationDoneStream =>
      _worldStateTypingAnimationDone.stream;

  final BehaviorSubject<bool> _resultTypingAnimationDone =
      BehaviorSubject<bool>();
  Sink<bool> get resultTypingAnimationDoneSink =>
      _resultTypingAnimationDone.sink;
  Stream<bool> get resultTypingAnimationDoneStream =>
      _resultTypingAnimationDone.stream;

  final BehaviorSubject<List<String>> _historyTextWidgets =
      BehaviorSubject<List<String>>();
  Sink<List<String>> get historyTextWidgetsSink => _historyTextWidgets.sink;
  Stream<List<String>> get historyTextWidgetsStream =>
      _historyTextWidgets.stream;

  final BehaviorSubject<bool> _inputLogsShown = BehaviorSubject<bool>();
  Sink<bool> get inputLogsShownSink => _inputLogsShown.sink;
  Stream<bool> get inputLogsShownStream => _inputLogsShown.stream;
  bool get isInputLogShown => _inputLogsShown.value;

  final BehaviorSubject<int> _bgImg = BehaviorSubject<int>();
  Sink<int> get bgImgSink => _bgImg.sink;
  Stream<int> get bgImgStream => _bgImg.stream;
  int _newBaseImg = 0;
  int get newBaseImg => _newBaseImg;

  MainBloc() {
    init();
  }

  init() {
    inputLogsShownSink.add(false);
    historyTextWidgetsSink.add([]);
    worldStatetypingAnimationDoneSink.add(false);
    getAllData();
  }

  getAllData() {
    Future.wait([
      _repository.getPersonData(),
      _repository.getEncounterListData(),
      _repository.getEncounterTableData(),
      _repository.getEncountersData(),
      _repository.getGuardTableData(),
      _repository.getGuardData(),
      _repository.getEncounterResultsData(),
      _repository.getResultItemsData(),
      _repository.getItemsData(),
      _repository.getItemTypesData(),
      _repository.getRecipesData(),
//      _repository.getSentenceStructureData(),
      _repository.getPersonStateData(),
      _repository.getFightDamageMessageData(),
      _repository.getInterfaceWords(),
      _repository.getEncounterTriggers()
    ]).then((dataList) {
      _personData = dataList[0];
      _encounterList = dataList[1];
      _encounterTable = dataList[2];
      _encountersData = dataList[3];
      _guardTableData = dataList[4];
      _guardsData = dataList[5];
      _encounterResultsData = dataList[6];
      _resultItemsData = dataList[7];
      _itemsData = dataList[8];
      _itemTypesData = dataList[9];
      _recipesData = dataList[10];
//      _sentenceStructureData = dataList[11];
      _personStateData = dataList[11];
      _fightDamageMessageData = dataList[12];
      _interfaceWordsData = dataList[13];
      _encounterTriggersData = dataList[14];
      personSink.add(_personData);
      int successCounter = 0;
      dataList.forEach((data) {
        if (data != null) successCounter++;
      });
      Globals().setPersonStateData(_personStateData);
      Globals().setInterfaceWordsData(_interfaceWordsData);
      _maxHealth = _personStateData.length - 1;
      if (successCounter == dataList.length)
        allDataLoadedSink.add(true);
      else
        allDataLoadedSink.add(false);
      initializeSparseWorld();
      logInput("Game Start...");
      _evaluateResultChanceTotal(_encounterResultsData);
    });
  }

  initializeSparseWorld() {
    List<Encounter> _draftedEncounters = List<Encounter>();
    if (_encounterTable != null) {
      _encounterTable.forEach((encounterTableItem) {
        List<EncounterListItem> _dimEncounterList =
            Utils.getEncounterListByTableId(
                _encounterList, encounterTableItem.id);
        List<EncounterListItem> encounterList = Utils.getEncounterListByTableId(
            _encounterList, encounterTableItem.id);
        encounterList.forEach((encounterListItem) {
          EncounterListItem randomEncounterListItem = _dimEncounterList[
              _chance.integer(min: 0, max: _dimEncounterList.length - 1)];
          _dimEncounterList.remove(randomEncounterListItem);
          GuardTableItem randomGuardTable = GuardTableItem();
          Guard guard;
          if (randomEncounterListItem.guardTableId != 0) {
            List<GuardTableItem> guardTable = Utils.getGuardTableByTableId(
                _guardTableData, randomEncounterListItem.guardTableId);
            randomGuardTable =
                guardTable[_chance.integer(min: 0, max: guardTable.length - 1)];
            if (randomGuardTable.guardId != null) {
              guard = Utils.getGuardbyId(_guardsData, randomGuardTable.guardId);
            }
          }
          Encounter draftedEncounter = Utils.getEncounterById(
              _encountersData, randomEncounterListItem.encounterName);
          draftedEncounter.guard = guard;
          _draftedEncounters.add(draftedEncounter);
        });
      });
    }
    if (_sparseWorldValue.droppedItems != null)
      _sparseWorldValue.droppedItems.clear();
    _sparseWorldValue.encounters = _draftedEncounters;
    updateSparseWorld();
    updatePerson();
  }

  Future<void> updateWidgetAction() async {
    historyTextWidgetsSink.add(_storedHistoryTexts);
    if (_storedHistoryTexts.first ==
        Globals().interfaceWord(InterfaceKey.PREVIOUSLY)) {
      await Future.delayed(Duration(seconds: 1));
    }
  }

  Future<void> storeText(String newHistoryText) async {
    _storedHistoryTexts.insert(0, newHistoryText);
  }

  updatePerson() {
    Person personValue = _person.value;
    int armor = 0;
    int damage = 0;
    if (personValue.head != null) {
      armor += checkArmor(personValue.head);
      damage += checkDamage(personValue.head);
    }
    if (personValue.handled != null) {
      armor += checkArmor(personValue.handled);
      damage += checkDamage(personValue.handled);
    }
    if (personValue.torso != null) {
      armor += checkArmor(personValue.torso);
      damage += checkDamage(personValue.torso);
    }
    if (personValue.feet != null) {
      armor += checkArmor(personValue.feet);
      damage += checkDamage(personValue.feet);
    }
    if (personValue.pet != null) {
      armor += checkArmor(personValue.pet);
      damage += checkDamage(personValue.pet);
    }
    personValue.armor = armor;
    personValue.damage = damage;
    personSink.add(_person.value);
  }

  int checkArmor(String itemRef) {
    Item item = Utils.getItemById(_itemsData, itemRef);
    ItemType itemType = Utils.getItemTypeById(_itemTypesData, item.type);
    return itemType.armor;
  }

  int checkDamage(String itemRef) {
    Item item = Utils.getItemById(_itemsData, itemRef);
    ItemType itemType = Utils.getItemTypeById(_itemTypesData, item.type);
    return itemType.damage;
  }

  updateSparseWorld() {
    sparseWorldSink.add(_sparseWorldValue);
  }

  void logInput(String text) {
    if (_inputLogs.value == null) {
      inputLogsSink.add("");
    }
    inputLogsSink
        .add(_inputLogs.value == "" ? "$text" : "${_inputLogs.value}\n$text");
  }

  void logResult(String text) {
    _lastResult = _currentResultLog.value;
    currentResultLogSink.add(null);
    Future.delayed(Duration(milliseconds: 200)).then((_) {
      currentResultLogSink.add(text);
    });
  }

  void clearInputLogs() {
    inputLogsSink.add("");
  }

  void resetData() {
    inputLogsSink.add("");
    allDataLoadedSink.add(null);
    resultTypingAnimationDoneSink.add(null);
    worldStatetypingAnimationDoneSink.add(null);
    currentResultLogSink.add(null);
    historyTextWidgetsSink.add([]);
    _storedHistoryTexts = [];
    personSink.add(null);
    init();
  }

  int checkWearPosition(String droppedItem) {
    try {
      Item item = Utils.getItemById(_itemsData, droppedItem);
      ItemType itemType = Utils.getItemTypeById(_itemTypesData, item.type);
      return itemType.wearPosition;
    } catch (e) {
      print("Error checking wear position of item $droppedItem.");
      print(e);
      return null;
    }
  }

  evaluateElementType(String elementType,
      {String handledItem,
      String droppedItem,
      Encounter encounter,
      int wearPosition}) {
    switch (elementType) {
      case Elements.SPARSE_WORLD:
        logInput("Select sparse world.");
        exploreSparseWorld();
        break;
      case Elements.HANDLED_ITEM:
        logInput("Select handled item $handledItem.");
        dropItem(handledItem, wearPosition);
        break;
      case Elements.ENCOUNTER:
        logInput("Select encounter ${encounter.id}.");
        if (encounter.guard == null) {
          encounterAction(encounter);
        } else {
          encounterActionDisabled(encounter.id);
        }
        break;
      case Elements.DROPPED_ITEM_EQUIP:
        logInput("Select dropped item $droppedItem.");
        equipItem(droppedItem, wearPosition);
        break;
      case Elements.DROPPED_ITEM_CRAFT:
        logInput("Select dropped item $droppedItem.");
        craftItem(droppedItem, handledItem);
        break;
      case Elements.GUARD_TAME:
        logInput(
            "Select guard ${encounter.guard.id} of encounter ${encounter.id}.");
        tameGuard(encounter);
        break;
      case Elements.GUARD_FIGHT:
        logInput(
            "Select guard ${encounter.guard.id} of encounter ${encounter.id}.");
        fightGuard(encounter);
        break;
    }
  }

  exploreSparseWorld() {
    String _textLog;
    List<Encounter> draftedEncounters = _sparseWorldValue.encounters;
    List<Encounter> unexploredEncounter =
        draftedEncounters.where((encounter) => !encounter.explored).toList();
    if (unexploredEncounter.isNotEmpty) {
      Encounter toExploreEncounter = unexploredEncounter.first;
      toExploreEncounter.explored = true;
      if (toExploreEncounter.message != null) {
        _textLog = toExploreEncounter.message;
      } else {
        _textLog =
            "${Globals().interfaceWord('announcement_explore_1')}${Utils.startsWithVowel(unexploredEncounter.first.id) ? "an" : "a"} ${unexploredEncounter.first.id}${Globals().interfaceWord('announcement_explore_2')}${unexploredEncounter.first.guard != null ? "${Globals().interfaceWord('announcement_explore_3')}${Utils.startsWithVowel(unexploredEncounter.first.guard.id) ? "an" : "a"} ${unexploredEncounter.first.guard.id}" : ""}${Globals().interfaceWord('announcement_explore_4')}";
      }
    } else {
      _textLog = Globals().interfaceWord('announcement_explore_all_explored');
    }
    logResult(_textLog);
    updateSparseWorld();

    ///sink sparse world
  }

  encounterAction(Encounter encounter) {
    EncounterResult _encounterResult = EncounterResult();
    _encounterResult = checkSpecialResult(encounter);
    List<String> itemsResult = _encounterResult.resultItemsId != 0
        ? Utils.getResultItemsById(
                _resultItemsData, _encounterResult.resultItemsId)
            .map((resultItem) => resultItem.itemId)
            .toList()
        : [] ?? [];
    if (_sparseWorldValue.droppedItems == null) {
      if (itemsResult.isNotEmpty) {
        _sparseWorldValue.droppedItems = itemsResult;
      }
    } else {
      _sparseWorldValue.droppedItems.addAll(itemsResult);
    }
    if (_person.value.energy - _encounterResult.energyConsumed > 0) {
      if (_person.value.energy - _encounterResult.energyConsumed >=
          _maxHealth) {
        _person.value.energy = _maxHealth;
      } else {
        _person.value.energy -= _encounterResult.energyConsumed;
      }
    }
    List<String> newItemsAsWordsList = List<String>();
    List<ResultItems> _resultItems = Utils.getResultItemsById(
        _resultItemsData, _encounterResult.resultItemsId);
    _resultItems.forEach((resultItem) {
      newItemsAsWordsList.add(
          "${Utils.startsWithVowel(resultItem.itemId) ? "an" : "a"} ${resultItem.itemId}");
      newItemsAsWordsList.add(_resultItems.length > 1 ? ", " : "");
    });
    if (_resultItems.length > 1) {
      newItemsAsWordsList.removeLast();
      newItemsAsWordsList.removeLast();
      newItemsAsWordsList.add("and ");
      newItemsAsWordsList.add(
          "${Utils.startsWithVowel(_resultItems.last.itemId) ? "an" : "a"} ${_resultItems.last.itemId}");
    }
    String newItemsAsWords = "";
    newItemsAsWordsList.forEach((newItemsAsWordsList) {
      newItemsAsWords = "$newItemsAsWords$newItemsAsWordsList";
    });
    String _resultLog;
    if (encounter.uses != null) {
      encounter.uses--;
      if (_encounterResult.message != null) {
        _resultLog = _encounterResult.message;
      } else {
        if (_resultItems.isNotEmpty) {
          _resultLog =
              "${Globals().interfaceWord('announcement_harvest_found_1')}${encounter.id}${Globals().interfaceWord('announcement_harvest_found_2')}$newItemsAsWords${Globals().interfaceWord('announcement_harvest_found_3')}";
        } else {
          _resultLog =
              "${Globals().interfaceWord('announcement_harvest_no_item_1')}${encounter.id}${Globals().interfaceWord('announcement_harvest_no_item_2')}";
        }
      }
      if (_encounterResult.encounterCreated != null) {
        _resultLog =
            "$_resultLog ${Globals().interfaceWord('announcement_harvest_cont_replaced_1')}${encounter.id}${Globals().interfaceWord('announcement_harvest_cont_replaced_2')}${_encounterResult.encounterCreated}${Globals().interfaceWord('announcement_harvest_cont_replaced_3')}";
        int toReplaceIndex = _sparseWorldValue.encounters.indexOf(encounter);
        _sparseWorldValue.encounters.removeAt(toReplaceIndex);
        Encounter _encounterCreated = Utils.getEncounterById(
            _encountersData, _encounterResult.encounterCreated);
        _encounterCreated.explored = true;
        _sparseWorldValue.encounters.insert(toReplaceIndex, _encounterCreated);
      } else {
        if (encounter.uses < 1) {
          _resultLog =
              "$_resultLog\n${Globals().interfaceWord('announcement_harvest_cont_used_1')}${encounter.id}${Globals().interfaceWord('announcement_harvest_cont_used_2')}";
          if (encounter.encounterReplacement != null) {
            int _toReplaceEncounterIndex =
                _sparseWorldValue.encounters.indexOf(encounter);
            Encounter newEncounterReplacement = Utils.getEncounterById(
                _encountersData, encounter.encounterReplacement);
            if (newEncounterReplacement != null) {
              newEncounterReplacement.explored = true;
            } else {
              print(
                  "Replacing Encounter Error. The encounterReplacement is not on encounter list.");
            }
            _sparseWorldValue.encounters
                .insert(_toReplaceEncounterIndex, newEncounterReplacement);
            updateSparseWorld();
          }
          _sparseWorldValue.encounters.remove(encounter);
        }
      }
    } else {
      _resultLog =
          "${Globals().interfaceWord('announcement_harvest_cont_used_1')}${encounter.id}${Globals().interfaceWord('announcement_harvest_cont_used_2')}";
      _sparseWorldValue.encounters.remove(encounter);
      if (_encounterResult.encounterCreated != null) {
        _resultLog =
            "$_resultLog ${Globals().interfaceWord('announcement_harvest_cont_replaced_1')}${encounter.id}${Globals().interfaceWord('announcement_harvest_cont_replaced_2')}${_encounterResult.encounterCreated}${Globals().interfaceWord('announcement_harvest_cont_replaced_3')}";
        _sparseWorldValue.encounters.add(Utils.getEncounterById(
            _encountersData, _encounterResult.encounterCreated));
      }
    }
    if (_encounterResult.backgroundImg != null) {
      bgImgSink.add(_encounterResult.backgroundImg);
    } else {
      bgImgSink.add(0);
    }
    logResult(_resultLog);
    updatePerson();
    updateSparseWorld();
  }

  EncounterResult checkSpecialResult(Encounter encounter) {
    EncounterTrigger matchedTrigger;
    List<EncounterTrigger> encounterTriggers =
        Utils.getEncounterTriggersByEncounterId(
            _encounterTriggersData, encounter.id);
    try {
      matchedTrigger = encounterTriggers.firstWhere((encounterTrigger) =>
          encounterTrigger.trigger == _person.value.energy.toString());
    } catch (e) {
      if (_person.value.handled != null) {
        Item item = Utils.getItemById(_itemsData, _person.value.handled);
        String itemType = Utils.getItemTypeById(_itemTypesData, item.type).name;
        try {
          matchedTrigger = encounterTriggers.firstWhere((encounterTrigger) =>
              encounterTrigger.trigger == _person.value.handled.toString());
        } catch (e) {
          try {
            matchedTrigger = encounterTriggers.firstWhere((encounterTrigger) =>
                itemType.contains(encounterTrigger.trigger ?? " "));
          } catch (e) {
            matchedTrigger = null;
          }
        }
      } else {
        matchedTrigger = null;
      }
    }
    if (matchedTrigger == null) {
      try {
        matchedTrigger = encounterTriggers
            .firstWhere((encounterTrigger) => encounterTrigger.trigger == null);
      } catch (e) {
        print(
            "Something wrong here. no null on encounter trigger group ${encounterTriggers.first.encounterId}");
      }
    }
    return setResult(matchedTrigger.result);
  }

  EncounterTrigger checkPossibleTrigger(Encounter encounter) {
    EncounterTrigger matchedTrigger; //null trigger
    List<EncounterTrigger> encounterTriggers =
        Utils.getEncounterTriggersByEncounterId(_encounterTriggersData,
            encounter.id); //get all encounter triggers by encounter id
    try {
      matchedTrigger = encounterTriggers.firstWhere((encounterTrigger) =>
          encounterTrigger.trigger ==
          _person.value.energy.toString()); //check if trigger is energy level
    } catch (e) {
      if (_person.value.handled != null) {
        //if person is not empty handed
        Item item = Utils.getItemById(_itemsData, _person.value.handled);
        String itemType = Utils.getItemTypeById(_itemTypesData, item.type).name;
        try {
          matchedTrigger = encounterTriggers.firstWhere((encounterTrigger) =>
              encounterTrigger.trigger ==
              _person.value.handled.toString()); //check if trigger is item
        } catch (e) {
          try {
            matchedTrigger = encounterTriggers.firstWhere((encounterTrigger) =>
                itemType.contains(encounterTrigger.trigger ??
                    " ")); //check if trigger is item type
          } catch (e) {
            matchedTrigger = null;
          }
        }
      } else {
        matchedTrigger = null;
      }
    }
    if (matchedTrigger == null) {
      try {
        matchedTrigger = encounterTriggers
            .firstWhere((encounterTrigger) => encounterTrigger.trigger == null);
      } catch (e) {
        print(
            "Something wrong here. no null on encounter trigger group ${encounterTriggers.first.encounterId}");
      }
    }
    return matchedTrigger;
  }

  EncounterResult setResult(int result) {
    List<EncounterResult> encounterResults =
        Utils.getEncounterResultsByGroupId(_encounterResultsData, result);
    EncounterResult draftedResult = draftResult(encounterResults);
    return draftedResult;
  }

  void _evaluateResultChanceTotal(List<EncounterResult> encounterResults) {
    List<List<EncounterResult>> groupedEncounterResultsList =
        groupBy(encounterResults, (encounterResult) => encounterResult.groupId)
            .values
            .toList();
    groupedEncounterResultsList.forEach((encounterResultsByGroupId) {
      try {
        double totalChance = 0;
        encounterResultsByGroupId.forEach((encounterResult) {
          totalChance += encounterResult.chance;
        });
        if (totalChance != 100.0) {
          logInput(
              "Warning: Encounter result group ${encounterResultsByGroupId.first.groupId} has a total of $totalChance chance.");
        }
      } catch (e) {
        print(e);
        logInput("Something wron on calculating chance");
      }
    });
  }

  EncounterResult draftResult(List<EncounterResult> encounterResults) {
    EncounterResult finalResult;
    encounterResults.sort((a, b) => a.chance.compareTo(b.chance));
    encounterResults
        .takeWhile((val) => finalResult == null)
        .forEach((encounterResult) {
      finalResult = tryChance(encounterResult);
    });
    if (finalResult == null) {
      if (encounterResults.isNotEmpty &&
          encounterResults.last.resultItemsId != null) {
        return encounterResults.last;
      } else
        return null;
    } else {
      return finalResult;
    }
  }

  EncounterResult tryChance(EncounterResult encounterResult) {
    if (_chance.boolean(likelihood: encounterResult.chance)) {
      return encounterResult;
    }
    return null;
  }

  encounterActionDisabled(String encounterName) {
    logResult(
        "${Globals().interfaceWord('announcement_harvest_blocked_1')}$encounterName${Globals().interfaceWord('announcement_harvest_blocked_2')}");
  }

  dropItem(String item, int wearPosition) {
    if (_person.value.handled == item) {
      _person.value.handled = null;
    }
    if (_person.value.head == item) {
      _person.value.head = null;
    }
    if (_person.value.torso == item) {
      _person.value.torso = null;
    }
    if (_person.value.feet == item) {
      _person.value.feet = null;
    }
    if (wearPosition != null) {
      if (wearPosition == WearPosition.HAND) {
        logResult(
            "${Globals().interfaceWord('announcement_drop_item_1')}${Utils.startsWithVowel(item) ? "an" : "a"} $item${Globals().interfaceWord('announcement_drop_item_2')}");
      } else {
        logResult(
            "${Globals().interfaceWord('announcement_take_off_item_1')}${Utils.startsWithVowel(item) ? "an" : "a"} $item${Globals().interfaceWord('announcement_take_off_item_2')}");
      }
    } else {
      logResult(
          "${Globals().interfaceWord('announcement_drop_item_1')}${Utils.startsWithVowel(item) ? "an" : "a"} $item${Globals().interfaceWord('announcement_drop_item_2')}");
    }
    if (_sparseWorldValue.droppedItems != null) {
      _sparseWorldValue.droppedItems.add(item);
    } else {
      _sparseWorldValue.droppedItems = [];
      _sparseWorldValue.droppedItems.add(item);
    }
    updatePerson();
    updateSparseWorld();
  }

  equipMechanics(String droppedItem, int wearPosition) {
    String beforeItem;
    switch (wearPosition) {
      case WearPosition.HAND:
        if (_person.value.handled != null) {
          //if something is on hand of person
          beforeItem = _person.value.handled;
          _sparseWorldValue.droppedItems.add(_person.value.handled);
        }
        _person.value.handled = droppedItem;
        break;
      case WearPosition.HEAD:
        if (_person.value.head != null) {
          //if something is on head of person
          beforeItem = _person.value.head;
          _sparseWorldValue.droppedItems.add(_person.value.head);
        }
        _person.value.head = droppedItem;
        break;
      case WearPosition.TORSO:
        if (_person.value.torso != null) {
          //if something is on torso of person
          beforeItem = _person.value.torso;
          _sparseWorldValue.droppedItems.add(_person.value.torso);
        }
        _person.value.torso = droppedItem;
        break;
      case WearPosition.FEET:
        if (_person.value.feet != null) {
          //if something is on feet of person
          beforeItem = _person.value.feet;
          _sparseWorldValue.droppedItems.add(_person.value.feet);
        }
        _person.value.feet = droppedItem;
        break;
      case WearPosition.PET:
        if (_person.value.pet != null) {
          //if something is on pet of person
          beforeItem = _person.value.pet;
          _sparseWorldValue.droppedItems.add(_person.value.pet);
        }
        _person.value.pet = droppedItem;
        break;
    }
    _sparseWorldValue.droppedItems.remove(droppedItem);
    return beforeItem;
  }

  equipItem(String droppedItem, int wearPosition) {
    String beforeItem = equipMechanics(droppedItem, wearPosition);
    if (beforeItem != null) {
      logResult(
          "${Globals().interfaceWord('announcement_equip_swap_1')}$beforeItem${Globals().interfaceWord('announcement_equip_swap_2')}$droppedItem${Globals().interfaceWord('announcement_equip_swap_3')}");
    } else {
      if (wearPosition != null) {
        if (wearPosition == WearPosition.HAND) {
          //if item is for hand
          logResult(
              "${Globals().interfaceWord('announcement_pick_up_new_1')}${Utils.startsWithVowel(droppedItem) ? "an" : "a"} $droppedItem${Globals().interfaceWord('announcement_pick_up_new_2')}");
        } else {
          //if item is not for hand
          logResult(
              "${Globals().interfaceWord('announcement_put_on_new_1')}${Utils.startsWithVowel(droppedItem) ? "an" : "a"} $droppedItem${Globals().interfaceWord('announcement_put_on_new_2')}");
        }
      } else {
        //if wear position is null, just to catch error.
        logResult(
            "${Globals().interfaceWord('announcement_equip_new_1')}${Utils.startsWithVowel(droppedItem) ? "an" : "a"} $droppedItem${Globals().interfaceWord('announcement_equip_new_2')}");
      }
    }
    updatePerson();
    updateSparseWorld();
  }

  craftItem(String droppedItem, String handledItem) {
    Recipe _foundRecipe;
    Item _carriedItem;
    try {
      try {
        if (handledItem == null) {
          _foundRecipe = _recipesData.firstWhere((recipe) =>
              recipe.carried == "empty handed" && recipe.target == droppedItem);
        } else {
          _carriedItem = Utils.getItemById(_itemsData, handledItem);
          _foundRecipe = _recipesData.firstWhere((recipe) =>
              recipe.carried == _carriedItem.id &&
              recipe.target == droppedItem);
        }
      } catch (e) {
        ItemType carriedItemType =
            Utils.getItemTypeById(_itemTypesData, _carriedItem.type);
        _foundRecipe = _recipesData.firstWhere((recipe) =>
            carriedItemType.name.contains(recipe.carried) &&
            recipe.target == droppedItem);
      }
      List<String> droppedItems = _sparseWorldValue.droppedItems;
      if (_foundRecipe.itemCreated != null) {
        droppedItems.add(_foundRecipe.itemCreated);
      }
      updateSparseWorld();
      if (_foundRecipe.message != null) {
        logResult(_foundRecipe.message);
      } else {
        if (handledItem != null) {
          logResult(
              "${Globals().interfaceWord('announcement_craft_handled_success_1')}$droppedItem${Globals().interfaceWord('announcement_craft_handled_success_2')}$handledItem${Globals().interfaceWord('announcement_craft_handled_success_3')}${_foundRecipe.itemCreated}${Globals().interfaceWord('announcement_craft_handled_success_4')}");
        } else {
          logResult(
              "${Globals().interfaceWord('announcement_craft_barehanded_success_1')}$droppedItem${Globals().interfaceWord('announcement_craft_barehanded_success_2')}${_foundRecipe.itemCreated}${Globals().interfaceWord('announcement_craft_barehanded_success_3')}");
        }
      }
      if (_foundRecipe.energyCost != null) {
        _personData.energy -= _foundRecipe.energyCost;
      }
      if (_foundRecipe.carriedConsumed == 1) {
        _personData.handled = null;
      }
      if (_foundRecipe.targetConsumed == 1) {
        droppedItems.remove(droppedItem);
      }
      updatePerson();
    } catch (e) {
      print(e.toString());
      if (handledItem != null) {
        logResult(
            "${Globals().interfaceWord('announcement_craft_handled_failed_1')}$droppedItem${Globals().interfaceWord('announcement_craft_handled_failed_2')}$handledItem${Globals().interfaceWord('announcement_craft_handled_failed_3')}");
      } else {
        logResult(
            "${Globals().interfaceWord('announcement_craft_barehanded_failed_1')}$droppedItem${Globals().interfaceWord('announcement_craft_barehanded_failed_2')}");
      }
    }
  }

  Color checkCraftableColor(String droppedItem, String handledItem) {
    Recipe _foundRecipe;
    Item _carriedItem;
    try {
      try {
        if (handledItem == null) {
          _foundRecipe = _recipesData.firstWhere((recipe) =>
              recipe.carried == "empty handed" && recipe.target == droppedItem);
        } else {
          _carriedItem = Utils.getItemById(_itemsData, handledItem);
          _foundRecipe = _recipesData.firstWhere((recipe) =>
              recipe.carried == _carriedItem.id &&
              recipe.target == droppedItem);
        }
      } catch (e) {
        ItemType carriedItemType =
            Utils.getItemTypeById(_itemTypesData, _carriedItem.type);
        _foundRecipe = _recipesData.firstWhere((recipe) =>
            carriedItemType.name.contains(recipe.carried) &&
            recipe.target == droppedItem);
      }
      return _foundRecipe.color;
    } catch (e) {
      return null;
    }
  }

  tameGuard(Encounter encounter) {
    String fullText;
    String handledItem = _person.value.handled;
    String guardMasterItem = encounter.guard.masterItemId;
    if (handledItem != null && handledItem == guardMasterItem) {
      fullText =
          "${Globals().interfaceWord('announcement_guard_tame_success_1')}${encounter.guard.id}${Globals().interfaceWord('announcement_guard_tame_success_2')}";
      _person.value.pet = encounter.guard.itemId;
      _person.value.handled = null;
      encounter.guard = null;
    } else {
      int damageToPerson =
          calculatedDamage(_person.value.armor, encounter.guard.damage);
      if (_person.value.energy - damageToPerson >= 0) {
        _person.value.energy = _person.value.energy - damageToPerson;
      } else {
        _person.value.energy = 0;
      }
      if (handledItem != null) {
        fullText =
            "${Globals().interfaceWord('announcement_guard_tame_handled_failed_1')}${encounter.guard.id}${Globals().interfaceWord('announcement_guard_tame_handled_failed_2')}$handledItem${Globals().interfaceWord('announcement_guard_tame_handled_failed_3')}";
      } else {
        fullText =
            "${Globals().interfaceWord('announcement_guard_tame_barehanded_failed_1')}${encounter.guard.id}${Globals().interfaceWord('announcement_guard_tame_barehanded_failed_2')}";
      }
      fullText =
          "$fullText${Globals().interfaceWord('announcement_guard_person_tame_damage_1')}$damageToPerson${Globals().interfaceWord('announcement_guard_person_tame_damage_2')}${_person.value.energy}${Globals().interfaceWord('announcement_guard_person_tame_damage_3')}";
    }
    logResult(fullText);
    updateSparseWorld();
    updatePerson();
  }

  Color checkGuardColor(Guard guard) {
    if (guard != null) {
      String handledItem = _person.value.handled;
      String guardMasterItem = guard.masterItemId;
      if (handledItem != null && handledItem == guardMasterItem) {
        return guard.color;
      }
    }
    return null;
  }

  fightGuard(Encounter encounter) {
    String fullText;
    Person person = _person.value;
    Guard guard = encounter.guard;
    FightDamageMessage _fightDamageMessage;
    if (guard.message != null) {
      fullText = guard.message;
    } else {
      fullText =
          "${Globals().interfaceWord('announcement_guard_fight_start_1')}${guard.id}${Globals().interfaceWord('announcement_guard_fight_start_2')}${encounter.id}${Globals().interfaceWord('announcement_guard_fight_start_3')}";
    }
    int newGuardEnergy;
    int damageToPerson = calculatedDamage(person.armor, guard.damage);
    int damageToGuard = calculatedDamage(guard.armor, person.damage);
    try {
      _fightDamageMessage = _fightDamageMessageData.singleWhere((damages) =>
          damages.personDamage == damageToGuard &&
          damages.guardDamage == damageToPerson &&
          encounter.guard.id == damages.guard);
      fullText = "$fullText\n${_fightDamageMessage.message}";
    } catch (_) {
      print("No custom damage message on this fight.");
    }
    fullText =
        "$fullText\n${Globals().interfaceWord('announcement_guard_fight_damages_1')}$damageToPerson${Globals().interfaceWord('announcement_guard_fight_damages_2')}\n${guard.id}${Globals().interfaceWord('announcement_guard_fight_damages_3')}$damageToGuard${Globals().interfaceWord('announcement_guard_fight_damages_4')}";
    if (person.energy - damageToPerson >= 0) {
      person.energy = person.energy - damageToPerson;
    } else {
      person.energy = 0;
    }
    if (guard.energy - damageToGuard >= 0) {
      newGuardEnergy = guard.energy - damageToGuard;
    } else {
      newGuardEnergy = 0;
    }
    fullText =
        "$fullText\n${Globals().interfaceWord('announcement_guard_fight_energy_1')}${person.energy}${Globals().interfaceWord('announcement_guard_fight_energy_2')}\n${guard.id}${Globals().interfaceWord('announcement_guard_fight_energy_3')}$newGuardEnergy${Globals().interfaceWord('announcement_guard_fight_energy_4')}";
    if (newGuardEnergy == 0) {
      fullText =
          "$fullText\n${Globals().interfaceWord('announcement_guard_died_1')}${encounter.guard.id}${Globals().interfaceWord('announcement_guard_died_2')}${encounter.id}${Globals().interfaceWord('announcement_guard_died_3')}";
      encounter.guard = null;
    } else {
      encounter.guard.energy = newGuardEnergy;
    }
    logResult(fullText);
    updateSparseWorld();
    updatePerson();
  }

  int calculatedDamage(int armor, int damage) {
    if (damage > armor)
      return damage - armor;
    else
      return 0;
  }

  listenMultiTaps() {
    _multiTapCount++;
    if (_multiTapCount == 1) {
      _multiTapTimer = Timer(Duration(milliseconds: 2500), () {
        _multiTapCount = 0;
      });
    }
    if (_multiTapCount == 5) {
      _multiTapTimer.cancel();
      toggleInputLog();
      _multiTapCount = 0;
    }
  }

  toggleInputLog() {
    inputLogsShownSink.add(!_inputLogsShown.value);
  }

  dispose() {
    _person.close();
    _inputLogs.close();
    _sparseWorld.close();
    _currentResultLog.close();
    _allDataLoaded.close();
    _coverAnimationDone.close();
    _worldStateTypingAnimationDone.close();
    _historyTextWidgets.close();
    _resultTypingAnimationDone.close();
    _inputLogsShown.close();
    _bgImg.close();
  }
}
