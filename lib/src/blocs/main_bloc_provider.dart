import 'package:flutter/widgets.dart';
import 'main_bloc.dart';

class MainBlocProvider extends InheritedWidget {
  final MainBloc mainBloc;

  MainBlocProvider({
    Key key,
    MainBloc mainBloc,
    Widget child,
  })  : mainBloc = mainBloc ?? MainBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static MainBloc of(BuildContext context) =>
      (context.dependOnInheritedWidgetOfExactType<MainBlocProvider>()).mainBloc;
}
