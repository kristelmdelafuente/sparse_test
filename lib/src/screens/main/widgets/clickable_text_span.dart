import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class ClickableTextSpan extends TextSpan {
  ClickableTextSpan({@required text, style, @required Function onTap})
      : super(
            text: text,
            style: style,
            recognizer: TapGestureRecognizer()..onTap = () => onTap());
}
