import 'package:flutter/material.dart';
import 'package:sparse/src/blocs/main_bloc.dart';

class CoverWidget extends StatefulWidget {
  final MainBloc mainBloc;
  final AnimationController coverAnimationController;
  CoverWidget({Key key, this.mainBloc, this.coverAnimationController})
      : super(key: key);
  @override
  _CoverWidgetState createState() => _CoverWidgetState();
}

class _CoverWidgetState extends State<CoverWidget>
    with SingleTickerProviderStateMixin {
//  AnimationController _controller;

  @override
  void initState() {
//    _controller = AnimationController(
//        duration: const Duration(milliseconds: 1500),
//        vsync: this
//    );
    widget.coverAnimationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        widget.mainBloc.coverAnimationDoneSink.add(true);
      }
    });
    super.initState();
  }

  @override
  void dispose() {
//    _controller.dispose();
    super.dispose();
  }

  Future<void> _playAnimation() async {
    try {
      await widget.coverAnimationController.forward().orCancel;
    } on TickerCanceled {
      // the animation got canceled, probably because we were disposed
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        initialData: false,
        stream: widget.mainBloc.allDataLoadedStream,
        builder: (context, snapshot) {
          return GestureDetector(
              onTap: snapshot.data == true
                  ? () {
                      _playAnimation();
                    }
                  : null,
              child: CoverPage(
                controller: widget.coverAnimationController.view,
                screenHeight: MediaQuery.of(context).size.height,
                allDataLoadedStream: widget.mainBloc.allDataLoadedStream,
              ));
        });
  }
}

class CoverPage extends StatelessWidget {
  final Animation<double> controller;
  final Animation<double> opacity;
  final Animation<double> coverHeight;
  final Animation<double> subTextHeight;
  final Stream<bool> allDataLoadedStream;
  final double screenHeight;
  CoverPage(
      {Key key, this.controller, this.screenHeight, this.allDataLoadedStream})
      : opacity = Tween<double>(
          begin: 1.0,
          end: 0.0,
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(
              0.0,
              0.300,
              curve: Curves.ease,
            ),
          ),
        ),
        coverHeight = Tween<double>(
          begin: screenHeight,
          end: 75.0,
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(
              0.125,
              0.550,
              curve: Curves.easeInOut,
            ),
          ),
        ),
        subTextHeight = Tween<double>(
          begin: 100,
          end: 0,
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(
              0.125,
              0.550,
              curve: Curves.easeInOut,
            ),
          ),
        ),
        super(key: key);

  Widget _buildAnimation(BuildContext context, Widget child) {
    return Container(
      height: coverHeight.value,
      color: Color(0xff1B1B1B),
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "SPARSE",
            style: TextStyle(
                color: Colors.white,
                fontFamily: "Proxima Nova",
                fontSize: 24,
                letterSpacing: 8),
          ),
          Opacity(
            opacity: opacity.value,
            child: Container(
                height: subTextHeight.value,
                padding: EdgeInsets.only(top: 50),
                child: StreamBuilder<bool>(
                    initialData: false,
                    stream: allDataLoadedStream,
                    builder: (context, snapshot) {
                      return AnimatedSwitcher(
                          duration: Duration(milliseconds: 1000),
                          child: snapshot.data == true
                              ? Text(
                                  "TAP TO START",
                                  style: TextStyle(
                                      color: Colors.white38,
                                      fontSize: 15,
                                      fontFamily: "Proxima Nova",
                                      letterSpacing: 5,
                                      fontWeight: FontWeight.w600),
                                )
                              : SizedBox());
                    })),
          )
        ],
      )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(animation: controller, builder: _buildAnimation);
  }
}
