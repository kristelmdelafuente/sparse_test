import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:sparse/src/screens/main/widgets/rsm_button/component/rsm_point.dart';

/// A component class that shows animated options around the main button.
///
/// This class accepts the following data.
/// [name] - The name of the main button.
/// [children] - The child MenuItem. Maximum of 4 widgets only. (These are exposed to the calling component for added control.)
/// [blur] - The amount of background blur (Gaussian).
/// [startDistance] - The start polar distance in which the options should start animating from.
/// [endDistance] - The end polar distance in which the options should stop animating to.
/// [animationDuration] - The duration of the animation in milliseconds.
/// [animationCurve] - The animation curve. Accepts values in Curves class (e.g. Curves.easeInSine, Curves.fastOutSlowIn, etc.)
///
class RSMButton extends StatefulWidget {
  RSMButton({
    Key key,
    this.name,
    this.children,
    this.blur,
    this.startDistance,
    this.endDistance,
    this.animationDuration,
    this.animationCurve,
    this.hasOverlay = false,
  }) : super(key: key);

  final Text name;
  final List<MenuItem> children;
  final double blur;
  final double startDistance;
  final double endDistance;
  final int animationDuration;
  final Curve animationCurve;
  final bool hasOverlay;

  @override
  _RSMButtonState createState() => _RSMButtonState();
}

class _RSMButtonState extends State<RSMButton>
    with SingleTickerProviderStateMixin {
  OverlayEntry overlayOption1, overlayOption2, overlayOption3, overlayOption4;
  OverlayEntry overlayBackground, overlayButton;
  AnimationController blurAnimationController;
  Animation<double> blurAnimation;
  GlobalKey keyButton = GlobalKey();
  bool isOptionsVisible = false;
  double blur = 0.0;
  double opacity = 0.0, opacityMain = 1;
  RSMPoint rsmPtItem1 = RSMPoint(0, 0);
  RSMPoint rsmPtItem2 = RSMPoint(0, 0);
  RSMPoint rsmPtItem3 = RSMPoint(0, 0);
  RSMPoint rsmPtItem4 = RSMPoint(0, 0);
  List<Widget> buttons = [];

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(afterLayout);
    super.initState();

    _initButtons();

    // Initialize animation handler for background blur. This is manually done since there is no support for animated blur.
    blurAnimationController = AnimationController(
        duration: Duration(milliseconds: widget.animationDuration),
        vsync: this);
    Animation curve = CurvedAnimation(
        parent: blurAnimationController, curve: widget.animationCurve);
    blurAnimation = Tween<double>(begin: 0, end: widget.blur).animate(curve);
    blurAnimation.addListener(() {
      Overlay.of(context).setState(() {});
    });
  }

  @override
  void dispose() {
    // Dispose animation handler
    blurAnimationController.dispose();
    super.dispose();
  }

  _initButtons() {
    widget.children?.forEach((MenuItem item) {
      buttons.add(FlatButton(
        child: Text(
          item.name,
          style: TextStyle(color: Colors.grey[900]),
        ),
        color: Colors.white,
        onPressed: () {
          hideOptions();
          item.onTap();
        },
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    // Show the main button
    return AnimatedOpacity(
      duration: Duration(milliseconds: widget.animationDuration),
      curve: widget.animationCurve,
      opacity: opacityMain,
      child: InkWell(
        child: widget.name,
        onTap: onTapMain,
        key: keyButton,
      ),
    );
  }

  /// Calculates the angular positions after the UI build executing is done
  void afterLayout(_) {
    final RenderBox renderButton = keyButton.currentContext.findRenderObject();
    final Size scrSize = MediaQuery.of(context).size;
    final Size btnSize = renderButton.size;
    final Offset btnPosition = renderButton.localToGlobal(Offset.zero);

    // Determine whether there is enough space around the button for the options to float around
    double rightDistance = scrSize.width - btnPosition.dx - btnSize.width;
    double bottomDistance = scrSize.height - btnPosition.dy - btnSize.height;
    bool isTopFree = btnPosition.dy > widget.endDistance;
    bool isLeftFree = btnPosition.dx > widget.endDistance;
    bool isRightFree = rightDistance > widget.endDistance;
    bool isBottomFree = bottomDistance > widget.endDistance;

    // Set allowable degree of freedom and layout options accordingly.
    if (isTopFree && isLeftFree && isRightFree && isBottomFree) {
      // center, specified rules are applied
      setState(() {
        if (widget.children.length == 1) {
          rsmPtItem1.setAngle(90);
        } else if (widget.children.length == 2) {
          rsmPtItem1.setAngle(90);
          rsmPtItem2.setAngle(270);
        } else if (widget.children.length == 3) {
          rsmPtItem1.setAngle(90);
          rsmPtItem2.setAngle(225);
          rsmPtItem3.setAngle(315);
        } else if (widget.children.length == 4) {
          rsmPtItem1.setAngle(90);
          rsmPtItem2.setAngle(180);
          rsmPtItem3.setAngle(270);
          rsmPtItem4.setAngle(360);
        }
      });
    } else if (!isTopFree && !isLeftFree) {
      // top left corner
      autoLayoutAngles(270, 360);
    } else if (!isTopFree && !isRightFree) {
      // top right corner
      autoLayoutAngles(180, 270);
    } else if (!isBottomFree && !isLeftFree) {
      // bottom left corner
      autoLayoutAngles(0, 90);
    } else if (!isBottomFree && !isRightFree) {
      // bottom right corner
      autoLayoutAngles(90, 180);
    } else if (!isTopFree) {
      // top
      autoLayoutAngles(180, 360);
    } else if (!isLeftFree) {
      // left
      autoLayoutAngles(270, 450);
    } else if (!isRightFree) {
      // right
      autoLayoutAngles(90, 270);
    } else if (!isBottomFree) {
      // bottom
      autoLayoutAngles(0, 180);
    }

    // Initialize the point radius
    rsmPtItem1.setRadius(widget.startDistance);
    rsmPtItem2.setRadius(widget.startDistance);
    rsmPtItem3.setRadius(widget.startDistance);
    rsmPtItem4.setRadius(widget.startDistance);
  }

  /// Distributes the options equally within the angular distance
  void autoLayoutAngles(double startAngle, double endAngle) {
    // Distribute the options evenly within the specified angular distance/range.
    double padStartAngle = startAngle;
    double padEndAngle = endAngle;
    double angularDist =
        (padEndAngle - padStartAngle) / (widget.children.length);

    double angle1 = padStartAngle + (angularDist * 0) + (angularDist / 2);
    double angle2 = padStartAngle + (angularDist * 1) + (angularDist / 2);
    double angle3 = padStartAngle + (angularDist * 2) + (angularDist / 2);
    double angle4 = padStartAngle + (angularDist * 3) + (angularDist / 2);

    rsmPtItem1.setAngle(angle1);
    rsmPtItem2.setAngle(angle2);
    rsmPtItem3.setAngle(angle3);
    rsmPtItem4.setAngle(angle4);
  }

  /// Procedure for showing the options
  void showOptions() async {
    // After overlay creation are done below, execute blur animation and option position changes
    WidgetsBinding.instance.addPostFrameCallback((_) {
      blurAnimationController.forward();
      Overlay.of(context).setState(() {
        opacity = 1.0;
        blur = widget.blur;
        rsmPtItem1.setRadius(widget.endDistance);
        rsmPtItem2.setRadius(widget.endDistance);
        rsmPtItem3.setRadius(widget.endDistance);
        rsmPtItem4.setRadius(widget.endDistance);
      });
    });

    // Hide the main button to avoid shadow effect
    setState(() {
      opacityMain = 0;
    });

    // Create the background blur and main button overlay
    overlayBackground = createOverlayBackground();
    overlayButton = createOverlayButton();

    // Create the options
    overlayOption1 =
        createOverlayEntry(rsmPtItem1, buttons.length > 0 ? buttons[0] : null);
    overlayOption2 =
        createOverlayEntry(rsmPtItem2, buttons.length > 1 ? buttons[1] : null);
    overlayOption3 =
        createOverlayEntry(rsmPtItem3, buttons.length > 2 ? buttons[2] : null);
    overlayOption4 =
        createOverlayEntry(rsmPtItem4, buttons.length > 3 ? buttons[3] : null);

    // Add the overlays to the context
    Overlay.of(context).insert(overlayBackground);
    Overlay.of(context).insert(overlayButton);
    Overlay.of(context).insert(overlayOption1);
    Overlay.of(context).insert(overlayOption2);
    Overlay.of(context).insert(overlayOption3);
    Overlay.of(context).insert(overlayOption4);
  }

  /// Procedure for hiding the options
  void hideOptions() async {
    // Execute reverse blur animation and option positions
    Overlay.of(context).setState(() {
      blurAnimationController.reverse();
      opacity = 0;
      blur = 0;
      rsmPtItem1.setRadius(widget.startDistance);
      rsmPtItem2.setRadius(widget.startDistance);
      rsmPtItem3.setRadius(widget.startDistance);
      rsmPtItem4.setRadius(widget.startDistance);
    });

    // Show the main button again
    setState(() {
      opacityMain = 1;
    });

    // Wait for the animation to be done before cleaning up
    await new Future.delayed(Duration(milliseconds: widget.animationDuration));

    // Remove the overlays from the context
    overlayBackground.remove();
    overlayButton.remove();
    overlayOption1.remove();
    overlayOption2.remove();
    overlayOption3.remove();
    overlayOption4.remove();
  }

  /// Tap handler of the main button
  void onTapMain() {
    // Toggle hide/show options
    if (isOptionsVisible) {
      hideOptions();
      isOptionsVisible = false;
    } else {
      showOptions();
      isOptionsVisible = true;
    }
  }

  /// Tap handler of the background blur
  void onTapBackground() {
    // Handle background tap
    hideOptions();
    isOptionsVisible = false;
  }

  /// OverlayEntry creator for the background blur
  OverlayEntry createOverlayBackground() {
    return OverlayEntry(
      builder: (context) => Positioned.fill(
        child: GestureDetector(
          onTap: onTapBackground,
          child: BackdropFilter(
            filter: ImageFilter.blur(
                sigmaX: blurAnimation.value, sigmaY: blurAnimation.value),
            child: Container(
              color: Colors.black.withOpacity(0),
            ),
          ),
        ),
      ),
    );
  }

  /// Overlay creator for the main button
  OverlayEntry createOverlayButton() {
    RenderBox renderBox = context.findRenderObject();
    var offset = renderBox.localToGlobal(Offset.zero);

    return OverlayEntry(
      builder: (context) => Positioned(
        left: offset.dx,
        top: offset.dy,
        child: AnimatedOpacity(
          curve: widget.animationCurve,
          duration: Duration(milliseconds: widget.animationDuration),
          opacity: opacity,
          child: FlatButton(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            child: widget.name,
            onPressed: onTapMain,
          ),
        ),
      ),
    );
  }

  /// Overlay creator for the options
  OverlayEntry createOverlayEntry(RSMPoint point, Widget child) {
    RenderBox renderBox = context.findRenderObject();
    var offset = renderBox.localToGlobal(Offset.zero);

    return OverlayEntry(
      builder: (context) => AnimatedPositioned(
        duration: Duration(milliseconds: widget.animationDuration),
        curve: widget.animationCurve,
        left: offset.dx + point.x,
        top: offset.dy - (point.y),
        child: AnimatedOpacity(
          curve: widget.animationCurve,
          duration: Duration(milliseconds: widget.animationDuration),
          opacity: opacity,
          child: child,
        ),
      ),
    );
  }
}

class MenuItem {
  String name;
  Function onTap;
  Color color;

  MenuItem({
    this.name,
    this.onTap,
    this.color,
  }) : super();
}
