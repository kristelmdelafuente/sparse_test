import 'dart:math' as math;

/// A container class that handles conversion from polar to cartesian coordinates
///
/// This class accepts an [angle] in degrees and a [radius]. Upon setting these, the class
/// automatically converts the polar coordinate to its equivalent cartesian coordinate.
/// The results are then stored in [x] and [y]. These values can be accessed directly.
///
class RSMPoint {
  double _angle;
  double _radius;
  double _radian;
  double x;
  double y;

  RSMPoint(double angle, double radius) {
    set(angle, radius);
  }

  void set(double angle, double radius) {
    _angle = angle;
    _radius = radius;
    _radian = (angle * math.pi) / 180;
    x = radius * math.cos(_radian);
    y = radius * math.sin(_radian);
  }

  void setAngle(double angle) {
    _angle = angle;
    _radian = (_angle * math.pi) / 180;
    x = _radius * math.cos(_radian);
    y = _radius * math.sin(_radian);
  }

  void setRadius(double radius) {
    _radius = radius;
    x = _radius * math.cos(_radian);
    y = _radius * math.sin(_radian);
  }
}
