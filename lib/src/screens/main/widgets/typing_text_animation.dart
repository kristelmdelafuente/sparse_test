import 'package:flutter/material.dart';
import 'package:sparse/src/utility.dart';

class TypingTextAnimation extends StatefulWidget {
  final String text;
  final TextStyle style;
  final Function onTap;
  final Function onFinished;
  final Duration typingSpeed;

  TypingTextAnimation(
      {Key key,
      @required this.text,
      this.style,
      this.onTap,
      this.onFinished,
      @required this.typingSpeed})
      : super(key: key);

  @override
  State createState() => new TypingTextAnimationState();
}

class TypingTextAnimationState extends State<TypingTextAnimation>
    with TickerProviderStateMixin {
  List<Widget> textAnimation = [];

  @override
  void initState() {
    super.initState();
    _startAnimation();
  }

  _startAnimation() {
    Future ft = Future(() {});
    for (int i = 0; i < widget.text.length - 1; i++) {
      var character = String.fromCharCode(widget.text.codeUnitAt(i));
      ft = ft.then((_) {
        return Future.delayed(widget.typingSpeed, () {
          AnimationController _controller = AnimationController(
            vsync: this,
            duration: const Duration(
              milliseconds: 800,
            ),
          );
          Animation<TextStyle> _animation = TextStyleTween(
            begin: Configs.TEXT_ANIMATION_FROM_STYLE,
            end: Configs.DEFAULT_TEXT_STYLE,
          ).animate(
            _controller,
          );

          _controller.forward().then((_) {
            if (i + 1 == widget.text.length - 1) {
              if (widget.onFinished != null) {
                widget.onFinished();
              }
            }
            _controller.dispose();
          });
          setState(() {
            textAnimation.add(
              AnimatedBuilder(
                animation: _animation,
                builder: (BuildContext context, Widget child) {
                  return Container(
                    height: 25,
                    child: Text(
                      character,
                      style: _animation.value,
                    ),
                  );
                },
              ),
            );
          });
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) => Wrap(
        children: textAnimation,
      );
}
