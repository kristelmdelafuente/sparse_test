import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sparse/src/blocs/main_bloc.dart';
import 'package:sparse/src/blocs/main_bloc_provider.dart';
import 'package:sparse/src/models/encounter.dart';
import 'package:sparse/src/models/encounter_trigger.dart';
import 'package:sparse/src/models/person.dart';
import 'package:sparse/src/models/item.dart';
import 'package:sparse/src/models/sparse_world.dart';
import 'package:sparse/src/screens/main/widgets/cover_widget.dart';
import 'package:sparse/src/screens/main/widgets/rsm_button/rsm_button.dart';
import 'package:sparse/src/screens/main/widgets/typing_text_animation.dart';
import 'package:sparse/src/utility.dart';
import 'package:collection/collection.dart';

class MainScreen extends StatelessWidget {
  MainScreen({Key key}) : super(key: key);

  static final mainBloc = MainBloc();
  @override
  Widget build(BuildContext context) {
    return MainBlocProvider(
      child: MainPage(),
      mainBloc: mainBloc,
    );
  }
}

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with TickerProviderStateMixin {
  MainBloc _mainBloc;
  AnimationController _coverAnimationController;
  RichText _worldStateRichText;
  String _worldStatePlainText;
  @override
  void initState() {
    _coverAnimationController = AnimationController(
        duration: const Duration(milliseconds: 1500), vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _coverAnimationController.dispose();
    _mainBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _mainBloc = MainBlocProvider.of(context);
    return Material(
      child: Center(
        child: Container(
          color: Color(0xff1B1B1B),
          child: SafeArea(
            child: Scaffold(
              body: Stack(
                children: <Widget>[
                  Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: StreamBuilder<int>(
                          initialData: 0,
                          stream: _mainBloc.bgImgStream,
                          builder: (context, bgImgSnapshot) {
                            return AnimatedSwitcher(
                              child: Container(
                                key: ValueKey(bgImgSnapshot.data),
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: AssetImage(
                                        "assets/images/${bgImgSnapshot.data}.png"),
                                  ),
                                ),
                              ),
                              duration: Duration(
                                  milliseconds: Configs.CROSSFADE_DURATION),
                            );
                          })),
                  StreamBuilder<bool>(
                      initialData: false,
                      stream: _mainBloc.coverAnimationDoneStream,
                      builder: (context, coverAnimationDoneSnapshot) {
                        if (coverAnimationDoneSnapshot.data) {
                          return Padding(
                            padding: const EdgeInsets.only(
                              left: 20,
                              right: 10,
                              top: 80,
                              bottom: 5.0,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  child: SingleChildScrollView(
                                    physics: NeverScrollableScrollPhysics(),
                                    child: AnimatedSize(
                                      vsync: this,
                                      alignment: Alignment.center,
                                      duration: Duration(milliseconds: 150),
                                      curve: Curves.fastOutSlowIn,
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            StreamBuilder<bool>(
                                                initialData: false,
                                                stream: _mainBloc
                                                    .inputLogsShownStream,
                                                builder: (context,
                                                    inputLogsShownSnapshot) {
                                                  return _buildEventLogs(
                                                      inputLogsShownSnapshot
                                                          .data);
                                                }),
                                            buildCurrentWorldState(),
                                            buildCurrentResult(),
                                            StreamBuilder<List<String>>(
                                                initialData: [],
                                                stream: _mainBloc
                                                    .historyTextWidgetsStream,
                                                builder: (context, snapshot) {
                                                  return ListView.builder(
                                                      shrinkWrap: true,
                                                      physics:
                                                          NeverScrollableScrollPhysics(),
                                                      itemCount:
                                                          snapshot.data.length,
                                                      itemBuilder:
                                                          (context, index) {
                                                        return Padding(
                                                          padding: EdgeInsets.only(
                                                              left: 5,
                                                              right: 5,
                                                              bottom: snapshot.data[
                                                                          index] ==
                                                                      Globals().interfaceWord(
                                                                          InterfaceKey
                                                                              .PREVIOUSLY)
                                                                  ? 5
                                                                  : 20),
                                                          child: Text(
                                                            snapshot
                                                                .data[index],
                                                            style: Configs
                                                                .DEFAULT_TEXT_STYLE
                                                                .merge(
                                                              TextStyle(
                                                                fontWeight: snapshot.data[
                                                                            index] ==
                                                                        Globals().interfaceWord(InterfaceKey
                                                                            .PREVIOUSLY)
                                                                    ? FontWeight
                                                                        .w600
                                                                    : FontWeight
                                                                        .normal,
                                                              ),
                                                            ),
                                                          ),
                                                        );
                                                      });
                                                }),
                                          ]),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        } else {
                          return SizedBox();
                        }
                      }),
                  Align(
                      alignment: Alignment.bottomLeft,
                      child: GestureDetector(
                        onTap: () {
                          _mainBloc.listenMultiTaps();
                        },
                        child: Container(
                            width: 50, height: 50, color: Colors.transparent),
                      )),
                  CoverWidget(
                      mainBloc: _mainBloc,
                      coverAnimationController: _coverAnimationController)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildEventLogs(bool logEventShown) {
    return Visibility(
      visible: logEventShown,
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(Globals().interfaceWord(InterfaceKey.LOG_BOX_NAME)),
              Row(
                children: <Widget>[
                  GestureDetector(
                      onTap: () {
                        _mainBloc.clearInputLogs();
                      },
                      child: Text(
                        Globals().interfaceWord(InterfaceKey.CLEAR_LOG),
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      )),
                  SizedBox(
                    width: 20,
                  ),
                  GestureDetector(
                      onTap: () {
                        _mainBloc.resetData();
                        reverseCoverAnimation();
                      },
                      child: Text(
                        Globals().interfaceWord(InterfaceKey.BUTTON_RESET),
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      )),
                ],
              ),
            ],
          ),
          SizedBox(height: 5),
          Container(
            padding: EdgeInsets.all(5),
            height: 100,
            width: double.infinity,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white70, width: 2),
                borderRadius: BorderRadius.all(Radius.circular(5))),
            child: SingleChildScrollView(
              reverse: true,
              child: StreamBuilder<String>(
                  stream: _mainBloc.inputLogsStream,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Text(snapshot.data, style: TextStyle(height: 1.1));
                    } else {
                      return SizedBox();
                    }
                  }),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> reverseCoverAnimation() async {
    try {
      await _coverAnimationController.reverse().orCancel;
    } on TickerCanceled {
      // the animation reverse got canceled, probably because we were disposed
    }
  }

  RichText _buildWorldStateText(
      {Person person, SparseWorld sparseWorld, bool textOnly = false}) {
    List<Encounter> exploredEncounters = sparseWorld?.encounters
            ?.where((encounter) => encounter.explored)
            ?.toList() ??
        [];
    return RichText(
        textAlign: TextAlign.start,
        text: TextSpan(
          style: Configs.DEFAULT_TEXT_STYLE,
          text: (person?.energy ?? 0) > 0
              ? ""
              : Globals().interfaceWord("structure_dead_state"),
          children: person.energy > 0
              ? [
                  TextSpan(text: Globals().personState(person.energy)),
                  TextSpan(
                      text: Globals().interfaceWord("structure_person_name")),
                  _stateEuippedItems(person, textOnly),
                  TextSpan(
                      text: " ${Globals().interfaceWord("structure_verb")} "),
                  _stateEncounters(exploredEncounters, textOnly),
                  _stateDroppedItems(
                      sparseWorld.droppedItems, person.handled, textOnly),
                  TextSpan(text: ""),
                ]
              : [],
        ));
  }

  Widget buildCurrentWorldState() {
    Widget itemWidget = StreamBuilder<Person>(
        stream: _mainBloc.personStream,
        builder: (context, personSnapshot) {
          if (personSnapshot.hasData) {
            return StreamBuilder<SparseWorld>(
                stream: _mainBloc.sparseWorldStream,
                builder: (context, sparseWorldSnapshot) {
                  if (sparseWorldSnapshot.hasData) {
                    _worldStateRichText = _buildWorldStateText(
                        person: personSnapshot.data,
                        sparseWorld: sparseWorldSnapshot.data);
                    _worldStatePlainText = _buildWorldStateText(
                            person: personSnapshot.data,
                            sparseWorld: sparseWorldSnapshot.data,
                            textOnly: true)
                        .text
                        .toPlainText();
                    return Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 20),
                      child: StreamBuilder<bool>(
                          stream: _mainBloc.worldStatetypingAnimationDoneStream,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return AnimatedSwitcher(
                                duration: Duration(milliseconds: 500),
                                child: !snapshot.data
                                    ? TypingTextAnimation(
                                        text: _worldStatePlainText,
                                        typingSpeed: Duration(
                                            milliseconds:
                                                _mainBloc.isInputLogShown
                                                    ? Configs.DEBUG_TYPING_SPEED
                                                    : Configs.TYPING_SPEED),
                                        onFinished: () => _mainBloc
                                            .worldStatetypingAnimationDoneSink
                                            .add(true),
                                      )
                                    : _worldStateRichText,
                              );
                            } else {
                              return SizedBox();
                            }
                          }),
                    );
                  } else {
                    return SizedBox();
                  }
                });
          } else {
            return SizedBox();
          }
        });
    return itemWidget;
  }

  Widget buildCurrentResult() {
    Widget itemWidget = StreamBuilder<String>(
        stream: _mainBloc.currentResultLogStream,
        builder: (context, currentResultLogStreamSnapshot) {
          if (currentResultLogStreamSnapshot.hasData) {
            return StreamBuilder<bool>(
                initialData: false,
                stream: _mainBloc.resultTypingAnimationDoneStream,
                builder: (context, snapshot) {
                  return snapshot.data
                      ? Padding(
                          padding: const EdgeInsets.only(
                              left: 5, right: 5, bottom: 20),
                          child: TypingTextAnimation(
                            text: currentResultLogStreamSnapshot.data,
                            typingSpeed: Duration(
                                milliseconds: _mainBloc.isInputLogShown
                                    ? Configs.DEBUG_TYPING_SPEED
                                    : Configs.TYPING_SPEED),
                            onFinished: () async {
                              _mainBloc.resultTypingAnimationDoneSink
                                  .add(false);
                              await _mainBloc.storeText(
                                  currentResultLogStreamSnapshot.data);
                              await _mainBloc.storeText(Globals()
                                  .interfaceWord(InterfaceKey.PREVIOUSLY));
                              await _mainBloc.updateWidgetAction();
                              _mainBloc.worldStatetypingAnimationDoneSink
                                  .add(false);
                            },
                          ),
                        )
                      : SizedBox();
                });
          } else {
            return SizedBox();
          }
        });
    return itemWidget;
  }

  TextSpan _stateEuippedItems(Person person, bool textOnly) {
    List<TextSpan> mainTextSpanList = List<TextSpan>();

    List<String> wornItems = List<String>();
    if (person.head != null) {
      wornItems.add(person.head);
    }
    if (person.torso != null) {
      wornItems.add(person.torso);
    }
    if (person.feet != null) {
      wornItems.add(person.feet);
    }
    if (wornItems.isNotEmpty) {
      mainTextSpanList.add(TextSpan(
          text: " ${Globals().interfaceWord("structure_equipped_item")} "));
      mainTextSpanList.add(_iterateEquippedItems(wornItems, textOnly));
    }
    if (person.handled != null) {
      mainTextSpanList.add(TextSpan(children: [
        TextSpan(
            text:
                "${wornItems.isNotEmpty ? ", and" : ""} ${Globals().interfaceWord("structure_handled_item")} "),
        _interactiveObject(
          person.handled,
          Elements.HANDLED_ITEM,
          textOnly,
          handledItem: person.handled,
        )
      ]));
    }

    if (person.pet != null) {
      mainTextSpanList.add(TextSpan(children: [
        TextSpan(
            text: wornItems.isNotEmpty || person.handled != null ? " and" : ""),
        TextSpan(
            text: " ${Globals().interfaceWord("structure_following_pet")} "),
        _interactiveObject(person.pet, Elements.HANDLED_ITEM, textOnly,
            handledItem: person.pet, disabled: true)
      ]));
    }

    return TextSpan(children: mainTextSpanList);
  }

  TextSpan _iterateEquippedItems(List<String> wornItems, bool textOnly) {
    int wornItemsCounter = 0;
    List<TextSpan> iteratedItems = wornItems.map((item) {
      wornItemsCounter++;
      return TextSpan(children: [
        _interactiveObject(item, Elements.HANDLED_ITEM, textOnly,
            handledItem: item, articled: false),
        TextSpan(
            text:
                wornItems.length > 1 && !(wornItemsCounter == wornItems.length)
                    ? ", "
                    : "")
      ]);
    }).toList();

    if (wornItems.length > 1) {
      iteratedItems.insert(wornItems.length - 1, TextSpan(text: "and "));
//      iteratedItems.addAll([
//        TextSpan(text: "and "),
//        _interactiveObject(wornItems.last, Elements.HANDLED_ITEM, handledItem: wornItems.last)
//      ]);
    }

    return TextSpan(children: iteratedItems);
  }

  TextSpan _stateEncounters(List<Encounter> encounters, bool textOnly) {
    List<TextSpan> encounterTextList = List<TextSpan>();
    if (encounters != null && encounters.isNotEmpty) {
      encounterTextList.addAll([
        TextSpan(
            text: "${Globals().interfaceWord("structure_has_encounter")} "),
        _interactiveObject(Globals().interfaceWord("structure_world_name"),
            Elements.SPARSE_WORLD, textOnly),
        TextSpan(
            text: " ${Globals().interfaceWord("structure_has_encounter_2")} ")
      ]);
      encounterTextList
          .addAll(_iterateAndQuantifyEncounters(encounters, textOnly));
    } else {
      encounterTextList.addAll([
        TextSpan(text: "${Globals().interfaceWord("structure_no_encounter")} "),
        _interactiveObject(Globals().interfaceWord("structure_world_name"),
            Elements.SPARSE_WORLD, textOnly),
        TextSpan(text: "${Globals().interfaceWord("structure_no_encounter_2")}")
      ]);
    }
    encounterTextList.add(TextSpan(text: "."));
    return TextSpan(children: encounterTextList);
  }

  TextSpan _stateDroppedItems(
      List<String> droppedItems, String handledItem, bool textOnly) {
    List<TextSpan> droppedItemsTextList = List<TextSpan>();
    if (droppedItems != null && droppedItems.isNotEmpty) {
      droppedItemsTextList.add(TextSpan(
          text: " ${Globals().interfaceWord("structure_dropped_items")} "));
      droppedItemsTextList
          .addAll(_iterateDroppedItems(droppedItems, handledItem, textOnly));
      droppedItemsTextList.add(TextSpan(
          text: " ${Globals().interfaceWord("structure_dropped_items_2")}."));
    } else {
      droppedItemsTextList.add(TextSpan(text: ""));
    }
    return TextSpan(children: droppedItemsTextList);
  }

  TextSpan _interactiveObject(String name, String elementType, bool textOnly,
      {String pluralName,
      int quantity,
      String handledItem,
      Encounter encounter,
      String droppedItem,
      Color color,
      bool disabled = false,
      bool articled = true}) {
    return TextSpan(
      children: [
        TextSpan(
            text:
                "${quantity != null && quantity > 1 ? "${Utils.numberToWord(quantity)} " : articled ? Utils.startsWithVowel(name) ? "an " : "a " : ""}"),
        textOnly
            ? TextSpan(
                text: "${quantity != null && quantity > 1 ? pluralName : name}",
              )
            : WidgetSpan(
                child: RSMButton(
                  name: Text(
                    "${quantity != null && quantity > 1 ? pluralName : name}",
                    style: Configs.DEFAULT_TEXT_STYLE.merge(
                      TextStyle(
                          color: color ?? Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  blur: 10.0,
                  startDistance: 0,
                  endDistance: 150,
                  animationDuration: 100,
                  animationCurve: Curves.fastOutSlowIn,
                  children: _menuOptions(context, elementType,
                      handledItem: handledItem,
                      droppedItem: droppedItem,
                      encounter: encounter),
                ),
              ),
      ],
    );
  }

  List<TextSpan> _iterateAndQuantifyEncounters(
      List<Encounter> exploredEncounters, bool textOnly) {
    int _quantityAppendCounter = 1;
    int _guardedEncounterCounter = 0;
    int _groupByIdLength;
    List<TextSpan> _textSpanList = List<TextSpan>();
    List<List<Encounter>> groupEncounterByIdAsList =
        groupBy(exploredEncounters, (exploredEncounter) => exploredEncounter.id)
            .values
            .toList(); //group explored encounters by name as list
    _groupByIdLength = groupEncounterByIdAsList.length;
    groupEncounterByIdAsList.forEach((encounterByIdList) {
      List<List<Encounter>> groupEncounterByIdAndGuardAsList = groupBy(
              encounterByIdList, (exploredEncounter) => exploredEncounter.guard)
          .values
          .toList(); //group the grouped explored encounters by guard as list.
      groupEncounterByIdAndGuardAsList.forEach((encounterByIdAndGuardList) {
        List<Encounter> guardedList =
            encounterByIdAndGuardList.where((e) => e.guard != null).toList();
        List<Encounter> unguardedList =
            encounterByIdAndGuardList.where((e) => e.guard == null).toList();
//        if (unguardedList.length>1){
//          _quantityAppendCounter++;
//        }
        if (guardedList.isNotEmpty && unguardedList.isNotEmpty) {
          _guardedEncounterCounter++;
        }
        Color guardTextColor =
            _mainBloc.checkGuardColor(encounterByIdAndGuardList.last.guard);
        EncounterTrigger possibleTrigger =
            _mainBloc.checkPossibleTrigger(encounterByIdAndGuardList.last);
        TextSpan encounterTextSpan = TextSpan(children: [
          _interactiveObject(
              encounterByIdAndGuardList.last.id, Elements.ENCOUNTER, textOnly,
              encounter: encounterByIdAndGuardList.last,
              quantity: encounterByIdAndGuardList.length,
              pluralName: encounterByIdAndGuardList.last.pluralName,
              color: possibleTrigger?.color),
          TextSpan(
              text: encounterByIdAndGuardList.last.guard != null
                  ? " guarded by "
                  : ""),
          encounterByIdAndGuardList.last.guard != null
              ? _interactiveObject(encounterByIdAndGuardList.last.guard.id,
                  Elements.GUARD, textOnly,
                  encounter: encounterByIdAndGuardList.last,
                  color: guardTextColor)
              : TextSpan(text: ""),
          TextSpan(
              text: _groupByIdLength > 1 &&
                      !(_groupByIdLength - 1 ==
                          groupEncounterByIdAsList.indexOf(encounterByIdList))
                  ? ", "
                  : "")
        ]);
        _textSpanList.add(encounterTextSpan);
      });
    });
    if (_groupByIdLength > 1) {
      _textSpanList.insert(
          _groupByIdLength - _quantityAppendCounter + _guardedEncounterCounter,
          TextSpan(text: "and "));
    }
    return _textSpanList;
  }

  List<TextSpan> _iterateDroppedItems(
      List<String> droppedItems, String handledItem, bool textOnly) {
    List<Item> mergedItems = List<Item>();
    droppedItems.forEach((droppedItem) {
      //each of droppedItem
      Color textColor = _mainBloc.checkCraftableColor(
          droppedItem, handledItem); //color of item text whatever the quantity
      List<String> identicalItems = droppedItems
          .where((whereDroppedItem) => whereDroppedItem == droppedItem)
          .toList();
      if (identicalItems.isNotEmpty) {
        try {
          Item existingItem = mergedItems.singleWhere((existingItem) =>
              existingItem.id ==
              identicalItems.first); //check if there's existing dup item
          existingItem.quantity++; //add quantity if there is
        } catch (_) {
          Item newItem = Utils.getItemById(_mainBloc.itemsData, droppedItem);
          newItem.textColor = textColor;
          mergedItems.add(newItem); //add new item
        }
      }
    });
    List<TextSpan> mainTextSpanList = List<TextSpan>();
    List<TextSpan> iterateTextSpanList = mergedItems
        .map((mergedItem) => TextSpan(children: [
              _interactiveObject(mergedItem.id, Elements.DROPPED_ITEM, textOnly,
                  pluralName: mergedItem.pluralName,
                  quantity: mergedItem.quantity,
                  droppedItem: mergedItem.id,
                  handledItem: handledItem,
                  color: mergedItem.textColor),
              TextSpan(text: mergedItems.length > 1 ? ", " : "")
            ]))
        .toList();
    mainTextSpanList
        .add(TextSpan(text: mergedItems.first.quantity > 1 ? "are " : "is "));
    mainTextSpanList.addAll(iterateTextSpanList);
    if (mergedItems.length > 1) {
      mainTextSpanList.removeLast();
      mainTextSpanList.addAll([
        TextSpan(text: "and "),
        _interactiveObject(mergedItems.last.id, Elements.DROPPED_ITEM, textOnly,
            pluralName: mergedItems.last.pluralName,
            quantity: mergedItems.last.quantity,
            droppedItem: mergedItems.last.id,
            handledItem: handledItem,
            color: mergedItems.last.textColor)
      ]);
    }
    return mainTextSpanList;
  }

//  void _toggleEventLog(){
//    setState(() {
//      _eventLogVisible = !_eventLogVisible;
//    });
//  }
  List<MenuItem> _menuOptions(BuildContext context, String elementType,
      {String handledItem, String droppedItem, Encounter encounter}) {
    switch (elementType) {
      case Elements.SPARSE_WORLD:
        return <MenuItem>[
          _menuItem(Globals().interfaceWord(InterfaceKey.WORLD_ACTION1), () {
            _mainBloc.evaluateElementType(elementType);
            _mainBloc.worldStatetypingAnimationDoneSink.add(null);

            ///hide world state
            _mainBloc.storeText(_worldStatePlainText);

            ///store history text
            _mainBloc.updateWidgetAction();

            ///display previous world state
            _mainBloc.resultTypingAnimationDoneSink.add(true);

            ///show result typing
          }),
        ];
        break;
      case Elements.HANDLED_ITEM:
        int wearPosition = _mainBloc.checkWearPosition(handledItem);
        String dropInterface;
        if (wearPosition != null) {
          if (wearPosition == 0) {
            dropInterface = Globals().interfaceWord("item_action_drop_handled");
          } else {
            dropInterface = Globals().interfaceWord("item_action_drop_worn");
          }
        } else {
          dropInterface =
              Globals().interfaceWord(InterfaceKey.HANDLED_ITEM_ACTION1);
        }
        return <MenuItem>[
          _menuItem(dropInterface, () {
            _mainBloc.evaluateElementType(elementType,
                handledItem: handledItem, wearPosition: wearPosition);
            _mainBloc.worldStatetypingAnimationDoneSink.add(null);

            ///hide world state
            _mainBloc.storeText(_worldStatePlainText);

            ///store history text
            _mainBloc.updateWidgetAction();

            ///display previous world state
            _mainBloc.resultTypingAnimationDoneSink.add(true);

            ///show result typing
          })
        ];
        break;
      case Elements.ENCOUNTER:
        EncounterTrigger possibleTrigger =
            _mainBloc.checkPossibleTrigger(encounter);
        return <MenuItem>[
          _menuItem(
              possibleTrigger?.interfaceText ??
                  Globals().interfaceWord(InterfaceKey.ENCOUNTER_ACTION1), () {
            _mainBloc.evaluateElementType(elementType, encounter: encounter);
            _mainBloc.worldStatetypingAnimationDoneSink.add(null);

            ///hide world state
            _mainBloc.storeText(_worldStatePlainText);

            ///store history text
            _mainBloc.updateWidgetAction();

            ///display previous world state
            _mainBloc.resultTypingAnimationDoneSink.add(true);

            ///show result typing
          })
        ];
        break;
      case Elements.DROPPED_ITEM:
        int wearPosition = _mainBloc.checkWearPosition(droppedItem);
        String equipInterface;
        if (wearPosition != null) {
          if (wearPosition == 0) {
            equipInterface =
                Globals().interfaceWord("item_action_equip_handled");
          } else {
            equipInterface = Globals().interfaceWord("item_action_equip_worn");
          }
        } else {
          equipInterface =
              Globals().interfaceWord(InterfaceKey.DROPPED_ITEM_ACTION1);
        }
        return <MenuItem>[
          _menuItem(equipInterface, () {
            _mainBloc.evaluateElementType(Elements.DROPPED_ITEM_EQUIP,
                droppedItem: droppedItem, wearPosition: wearPosition);
            _mainBloc.worldStatetypingAnimationDoneSink.add(null);

            ///hide world state
            _mainBloc.storeText(_worldStatePlainText);

            ///store history text
            _mainBloc.updateWidgetAction();

            ///display previous world state
            _mainBloc.resultTypingAnimationDoneSink.add(true);

            ///show result typing
          }),
          _menuItem(Globals().interfaceWord(InterfaceKey.DROPPED_ITEM_ACTION2),
              () {
            _mainBloc.evaluateElementType(Elements.DROPPED_ITEM_CRAFT,
                handledItem: handledItem, droppedItem: droppedItem);
            // Navigator.pop(context);
            _mainBloc.worldStatetypingAnimationDoneSink.add(null);

            ///hide world state
            _mainBloc.storeText(_worldStatePlainText);

            ///store history text
            _mainBloc.updateWidgetAction();

            ///display previous world state
            _mainBloc.resultTypingAnimationDoneSink.add(true);

            ///show result typing
          }),
        ];
        break;
      case Elements.GUARD:
        return <MenuItem>[
          _menuItem(Globals().interfaceWord(InterfaceKey.GUARD_ACTION1), () {
            _mainBloc.evaluateElementType(Elements.GUARD_TAME,
                encounter: encounter);
            _mainBloc.worldStatetypingAnimationDoneSink.add(null);

            ///hide world state
            _mainBloc.storeText(_worldStateRichText.text.toPlainText());

            ///store history text
            _mainBloc.updateWidgetAction();

            ///display previous world state
            _mainBloc.resultTypingAnimationDoneSink.add(true);

            ///show result typing
          }),
          _menuItem(Globals().interfaceWord(InterfaceKey.GUARD_ACTION2), () {
            _mainBloc.evaluateElementType(Elements.GUARD_FIGHT,
                encounter: encounter);
            _mainBloc.worldStatetypingAnimationDoneSink.add(null);

            ///hide world state
            _mainBloc.storeText(_worldStatePlainText);

            ///store history text
            _mainBloc.updateWidgetAction();

            ///display previous world state
            _mainBloc.resultTypingAnimationDoneSink.add(true);

            ///show result typing
          }),
        ];
        break;
      default:
        return [];
        break;
    }
  }

  MenuItem _menuItem(String name, Function onTap, {Color color}) {
    return MenuItem(name: name, onTap: onTap, color: color);
  }
}
