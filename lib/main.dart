import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'src/screens/main/main_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          brightness: Brightness.dark,
          backgroundColor: MaterialColor(0xFF333333, bgColorMap),
          primarySwatch: MaterialColor(0xFF000000, primaryColorMap),
          fontFamily: 'Helvetica'),
      home: MainScreen(),
    );
  }

  final Map<int, Color> bgColorMap = {
    50: Color.fromRGBO(51, 51, 51, .1),
    100: Color.fromRGBO(51, 51, 51, .2),
    200: Color.fromRGBO(51, 51, 51, .3),
    300: Color.fromRGBO(51, 51, 51, .4),
    400: Color.fromRGBO(51, 51, 51, .5),
    500: Color.fromRGBO(51, 51, 51, .6),
    600: Color.fromRGBO(51, 51, 51, .7),
    700: Color.fromRGBO(51, 51, 51, .8),
    800: Color.fromRGBO(51, 51, 51, .9),
    900: Color.fromRGBO(51, 51, 51, 1),
  };
  final Map<int, Color> primaryColorMap = {
    50: Color.fromRGBO(0, 0, 0, .1),
    100: Color.fromRGBO(0, 0, 0, .2),
    200: Color.fromRGBO(0, 0, 0, .3),
    300: Color.fromRGBO(0, 0, 0, .4),
    400: Color.fromRGBO(0, 0, 0, .5),
    500: Color.fromRGBO(0, 0, 0, .6),
    600: Color.fromRGBO(0, 0, 0, .7),
    700: Color.fromRGBO(0, 0, 0, .8),
    800: Color.fromRGBO(0, 0, 0, .9),
    900: Color.fromRGBO(0, 0, 0, 1),
  };
}
